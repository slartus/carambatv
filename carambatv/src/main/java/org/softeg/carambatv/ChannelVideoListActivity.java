package org.softeg.carambatv;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.MenuItem;

import org.softeg.carambatvapi.ChannelItem;

/*
 * Created by Артём on 26.05.2014.
 */
public class ChannelVideoListActivity extends FragmentActivity {
    private Bundle mFragmentStates = new Bundle();

    public static void show(Context context, ChannelItem channelItem) {
        Intent intent = new Intent(context, ChannelVideoListActivity.class);
        intent.putExtra(ChannelVideoListFragment.CHANNEL_ITEM_KEY, (Parcelable) channelItem);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(android.view.Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.channel_video_activity);

        if (getActionBar() != null) {
            getActionBar().setDisplayShowHomeEnabled(true);
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        PlaceholderFragment fragment = (PlaceholderFragment) getSupportFragmentManager().findFragmentById(R.id.container);

        if (getIntent().getExtras() != null)
            mFragmentStates.putAll(getIntent().getExtras());

        if (savedInstanceState != null)
            mFragmentStates.putAll(savedInstanceState);

        ChannelItem channelItem = mFragmentStates.getParcelable(ChannelVideoListFragment.CHANNEL_ITEM_KEY);
        getActionBar().setTitle(!TextUtils.isEmpty(channelItem.getTitle())?channelItem.getTitle():channelItem.getTitleFromUrl());

        if (fragment == null) {
            fragment = new ChannelVideoListFragment();
            fragment.setArguments(mFragmentStates);
        }
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putAll(mFragmentStates);
        super.onSaveInstanceState(outState);

    }
}
