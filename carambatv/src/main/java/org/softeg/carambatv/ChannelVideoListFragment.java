package org.softeg.carambatv;

import android.os.Bundle;
import android.text.TextUtils;

import org.softeg.carambatvapi.CarambaApi;
import org.softeg.carambatvapi.ChannelItem;
import org.softeg.carambatvapi.VideoItem;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by Артём on 26.05.2014.
 */
public class ChannelVideoListFragment extends VideoListFragment {
    public static final String UNIQUE_NAME = "channel";
    public static final String CHANNEL_ITEM_KEY = "CHANNEL_ITEM_KEY";

    private ChannelItem mChannelItem;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null)
            mChannelItem = getArguments().getParcelable(CHANNEL_ITEM_KEY);
        if (savedInstanceState != null)
            mChannelItem = getArguments().getParcelable(CHANNEL_ITEM_KEY);
    }

    @Override
    protected void deliveryResult(List<VideoItem> result, Boolean mRefresh) {
        super.deliveryResult(result, mRefresh);

        if (getActivity() != null) {
            getActivity().getActionBar().setTitle(!TextUtils.isEmpty(mChannelItem.getTitle()) ? mChannelItem.getTitle() : mChannelItem.getTitleFromUrl());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle args) {
        args.putParcelable(CHANNEL_ITEM_KEY, mChannelItem);
        super.onSaveInstanceState(args);
    }

    @Override
    protected List<VideoItem> loadData(Boolean mRefresh) throws Exception {
        mListInfo.setFrom(mRefresh ? 0 : mData.size());
        if (mRefresh)
            mListInfo.setMax(0);

        return CarambaApi.getChannelVideoList(mChannelItem, mListInfo);
    }

    @Override
    public String getUniqueName() {
        return UNIQUE_NAME;
    }

    @Override
    protected List<VideoItem> loadCache() {
        return new ArrayList<>();
    }

    @Override
    protected void saveCache(ArrayList<VideoItem> items) {

    }
}
