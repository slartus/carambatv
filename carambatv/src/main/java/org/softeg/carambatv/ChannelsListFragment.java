package org.softeg.carambatv;

import android.app.ActionBar;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.softeg.carambatv.common.Log;
import org.softeg.carambatvapi.CarambaApi;
import org.softeg.carambatvapi.ChannelItem;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.Options;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

/*
 * Created by Артём on 26.05.2014.
 */
public class ChannelsListFragment extends PlaceholderFragment implements ListView.OnItemClickListener {
    public static final String UNIQUE_NAME = "channels";
    private GridView mListView;
    private ListAdapter mAdapter;
    protected ArrayList<ChannelItem> mData = new ArrayList<>();


    public static final String LIST_VIEW_DATA_KEY = "LIST_VIEW_DATA_KEY";
    public static final String LIST_VIEW_POSITION_KEY = "LIST_VIEW_POSITION_KEY";

    private Task mTask = null;
    private LoadCacheTask mCacheTask = null;

    private String mFilter = null;
    protected PullToRefreshLayout mPullToRefreshLayout;
    private void initFilter() {
        ActionBar actionBar = getActivity().getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);


        final String[] data = new String[]{"Все", "Игры", "Музыка", "Мультфильмы", "Развлечения", "Сериалы", "Спорт", "Технологии", "Юмор"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(actionBar.getThemedContext(),
                android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        actionBar.setListNavigationCallbacks(adapter, new ActionBar.OnNavigationListener() {
            @Override
            public boolean onNavigationItemSelected(int i, long l) {

                if (i == 0)
                    mFilter = null;
                else
                    mFilter = data[i];
                mAdapter.getFilter().filter(mFilter);
                return true;
            }
        });
    }

    @Override
    public String getUniqueName() {
        return UNIQUE_NAME;
    }

    @Override
    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.channels_list, container, false);
        assert v != null;
        mListView = (GridView) v.findViewById(R.id.gridView1);
        mListView.setOnItemClickListener(this);
        mListView.setEmptyView(v.findViewById(android.R.id.empty));
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initFilter();
        registerForContextMenu(mListView);

        mAdapter = new ListAdapter(getActivity(), mData);

        mListView.setAdapter(mAdapter);

        Bundle args = new Bundle();
        if (getArguments() != null)
            args.putAll(getArguments());
        if (savedInstanceState != null)
            args.putAll(savedInstanceState);

        if (args.containsKey(LIST_VIEW_DATA_KEY)) {
            ArrayList<ChannelItem> data = args.getParcelableArrayList(LIST_VIEW_DATA_KEY);
            if (data != null)
                mData.addAll(data);
            mAdapter.getFilter().filter(mFilter);

            mListView.smoothScrollToPosition(args.getInt(LIST_VIEW_POSITION_KEY, 0));
        } else {
            mCacheTask = new LoadCacheTask();
            mCacheTask.execute();
        }


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPullToRefreshLayout = createPullToRefreshLayout(view);
    }

    protected PullToRefreshLayout createPullToRefreshLayout(View view) {
        // We need to create a PullToRefreshLayout manually
        PullToRefreshLayout pullToRefreshLayout = (PullToRefreshLayout) view.findViewById(R.id.ptr_layout);
        assert pullToRefreshLayout!=null;
        // We can now setup the PullToRefreshLayout
        ActionBarPullToRefresh.from(getActivity())
                .options(Options.create().scrollDistance(0.3f).refreshOnUp(true).build())
                        // We need to insert the PullToRefreshLayout into the Fragment's ViewGroup
                .allChildrenArePullable()

                        // We can now complete the setup as desired
                .listener(new OnRefreshListener() {
                    @Override
                    public void onRefreshStarted(View view) {
                        loadData(true);
                    }
                })
                .setup(pullToRefreshLayout);
        return pullToRefreshLayout;
    }

    @Override
    protected void reloadData() {
        loadData(true);
    }

    @Override
    public void onSaveInstanceState(Bundle args) {
        args.putParcelableArrayList(LIST_VIEW_DATA_KEY, mData);

        args.putInt(LIST_VIEW_POSITION_KEY, mListView.getFirstVisiblePosition());
        super.onSaveInstanceState(args);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        setLoading(false);

        if (mCacheTask != null)
            mCacheTask.cancel(false);
        if (mTask != null)
            mTask.cancel(null);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ChannelItem item = (ChannelItem) mAdapter.getItem((int) l);
        ChannelVideoListActivity.show(getActivity(), item);
    }

    private void deliveryResult(List<ChannelItem> result, Boolean mRefresh) {
        if (mRefresh)
            mData.clear();
        mData.addAll(result);

        mAdapter.getFilter().filter(mFilter);
        try {
            saveCache(mData);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private List<ChannelItem> loadData() throws Exception {
        return CarambaApi.getChannels();
    }

    private void setLoading(boolean loading) {
        if (getActivity() == null) return;

        mPullToRefreshLayout.setRefreshing(loading);
    }

    private void startLoad() {
        if (mTask != null)
            return;
        loadData(true);
    }

    public void loadData(final boolean isRefresh) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                mTask = new Task(isRefresh);
                mTask.execute();
            }
        };
        if (mTask != null && mTask.getStatus() != AsyncTask.Status.FINISHED)
            mTask.cancel(runnable);
        else {
            runnable.run();
        }
    }


    private class ListAdapter extends BaseAdapter implements Filterable {
        private final LayoutInflater mInflater;
        protected ArrayList<ChannelItem> mData;
        protected ArrayList<ChannelItem> mFilteredData = new ArrayList<>();
        private Context mContext;

        public ListAdapter(Context context, ArrayList<ChannelItem> data) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mData = data;
            mFilteredData = mData;
            mContext = context;

        }

        @Override
        public int getCount() {
            return mFilteredData == null ? 0 : mFilteredData.size();
        }

        @Override
        public Object getItem(int i) {
            return mFilteredData.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public android.view.View getView(int position, android.view.View view, android.view.ViewGroup parent) {
            final ViewHolder holder;
            if (view == null) {
                view = mInflater.inflate(R.layout.channel_item, parent, false);
                holder = new ViewHolder();
                assert view != null;
                holder.image = (ImageView) view.findViewById(R.id.imageView);
                holder.progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
                holder.imageError = view.findViewById(R.id.image_error);
                holder.tagTitleTextView = (TextView) view.findViewById(R.id.tagTitleTextView);
                holder.titleTextView = (TextView) view.findViewById(R.id.titleTextView);
                view.setTag(holder);

            } else {
                holder = (ViewHolder) view.getTag();
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.imageError.setVisibility(View.GONE);
            }
            ChannelItem item = (ChannelItem) getItem(position);

            if (item.getImageUrl() == null) {
                holder.progressBar.setVisibility(View.GONE);
            } else {
                try {
                    Picasso.with(mContext).load(item.getImageUrl().toString())
                            .into(holder.image, new Callback() {
                                @Override
                                public void onSuccess() {
                                    holder.progressBar.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    holder.progressBar.setVisibility(View.GONE);
                                    holder.imageError.setVisibility(View.VISIBLE);
                                }
                            });
                } catch (Throwable ex) {
                    holder.progressBar.setVisibility(View.GONE);
                    holder.imageError.setVisibility(View.VISIBLE);
                    ex.printStackTrace();
                }
            }

            holder.titleTextView.setText(item.getTitle());
            holder.tagTitleTextView.setText(item.getTagTitle());
            return view;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mFilteredData = (ArrayList<ChannelItem>) results.values;
                    notifyDataSetChanged();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    final FilterResults filterResults = new FilterResults();
                    ArrayList<ChannelItem> filteredResults = new ArrayList<>();

                    if (constraint != null) {

                        for (ChannelItem item : mData) {
                            if (item.getTagTitle().equals(constraint))
                                filteredResults.add(item);
                        }


                    } else {
                        filteredResults.addAll(mData);
                    }
                    filterResults.values = filteredResults;
                    return filterResults;
                }
            };
        }

        class ViewHolder {
            ImageView image;
            View imageError;
            TextView tagTitleTextView;
            TextView titleTextView;
            ProgressBar progressBar;
        }
    }

    public class Task extends AsyncTask<Boolean, Void, List<ChannelItem>> {
        protected Boolean mRefresh;
        private Runnable onCancelAction;
        protected Throwable mEx;

        public Task(Boolean refresh) {
            mRefresh = refresh;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            setLoading(true);
        }

        public void cancel(Runnable runnable) {
            onCancelAction = runnable;

            cancel(false);
        }

        @Override
        protected List<ChannelItem> doInBackground(Boolean[] p1) {
            try {

                return loadData();

            } catch (Throwable e) {
                mEx = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<ChannelItem> result) {
            super.onPostExecute(result);
            if (result != null && !isCancelled()) {
                deliveryResult(result, mRefresh);
            }
            setLoading(false);
            if (mEx != null) {
                Log.e(getActivity(), mEx);
            }
        }

        @Override
        protected void onCancelled(List<ChannelItem> result) {
            if (onCancelAction != null)
                onCancelAction.run();
        }

        @Override
        protected void onCancelled() {
            if (onCancelAction != null)
                onCancelAction.run();
        }
    }

    public class LoadCacheTask extends AsyncTask<Boolean, Void, List<ChannelItem>> {
        private Throwable mEx;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setLoading(true);
        }

        @Override
        protected List<ChannelItem> doInBackground(Boolean[] p1) {
            try {

                return loadCache();

            } catch (Throwable e) {
                mEx = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<ChannelItem> result) {
            super.onPostExecute(result);

            if (mEx != null) {
                mEx.printStackTrace();
            }

            if (!isCancelled()) {
                deliveryCache(result);

                startLoad();
            }
        }

        @Override
        protected void onCancelled(List<ChannelItem> result) {
            setLoading(false);
        }

        @Override
        protected void onCancelled() {
            setLoading(false);
        }

    }

    private List<ChannelItem> loadCache() throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = null;
        FileInputStream fileInputStream = null;
        try {

            fileInputStream = getActivity().openFileInput(getUniqueName() + ".tmp");
            objectInputStream = new ObjectInputStream(fileInputStream);
            return (ArrayList<ChannelItem>) objectInputStream.readObject();
        } finally {
            if (objectInputStream != null)
                objectInputStream.close();
            if (fileInputStream != null)
                fileInputStream.close();
        }
    }

    private void saveCache(ArrayList<ChannelItem> items) throws IOException, ClassNotFoundException {
        if (items == null || items.size() == 0)
            return;
        ObjectOutputStream objectOutputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = getActivity().openFileOutput(getUniqueName() + ".tmp", Context.MODE_PRIVATE);
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(items);
            objectOutputStream.close();
        } finally {
            if (objectOutputStream != null)
                objectOutputStream.close();
            if (fileOutputStream != null)
                fileOutputStream.close();
        }
    }

    protected void deliveryCache(List<ChannelItem> cacheList) {
        mData.clear();
        if (cacheList != null) {
            mData.addAll(cacheList);
        }

        mAdapter.getFilter().filter(mFilter);
    }
}
