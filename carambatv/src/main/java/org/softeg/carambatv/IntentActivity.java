package org.softeg.carambatv;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;

import org.softeg.carambatv.common.Log;
import org.softeg.carambatv.db.VideoItemsContract;
import org.softeg.carambatv.db.VideoItemsHistoryContract;
import org.softeg.carambatvapi.CarambaApi;
import org.softeg.carambatvapi.CarambaUrlType;
import org.softeg.carambatvapi.ChannelItem;
import org.softeg.carambatvapi.Quality;
import org.softeg.carambatvapi.VideoItem;
import org.softeg.videoplayer.PlayerActivity;
import org.softeg.videoplayer.VideoInfo;

/*
 * Created by Артём on 27.05.2014.
 */
public class IntentActivity extends Activity {

    public static final String CHOOSE_PLAYER_KEY = "CHOOSE_PLAYER_KEY";
    private Bundle mArgs = new Bundle();
    View progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.intent_activity);
        progressBar = findViewById(R.id.progressBar);

        if (getIntent().getExtras() != null)
            mArgs.putAll(getIntent().getExtras());
        if (savedInstanceState != null)
            mArgs.putAll(savedInstanceState);
        try {
            if (mArgs.containsKey(PlayerActivity.VIDEO_ITEM_KEY)) {
                VideoItem videoItem = mArgs.getParcelable(PlayerActivity.VIDEO_ITEM_KEY);
                assert videoItem != null;
                if (!TextUtils.isEmpty(videoItem.getDefaultVideoUrl())) {
                    startVideo(videoItem);
                    return;
                } else {
                    mTask = new LoadVideoItemTask(this, videoItem);
                    mTask.execute();
                    return;
                }
            }

        } catch (Throwable e) {
            Log.e(this, e);
        }

        if (checkIntent()) {
            finish();
        }
    }

    public static void show(Context context, VideoItem videoItem) {
        Intent intent = new Intent(context, IntentActivity.class);

        intent.putExtra(PlayerActivity.VIDEO_ITEM_KEY, (Parcelable) videoItem);
        context.startActivity(intent);
    }

    public static void showChooser(Context context, VideoItem videoItem) {
        Intent intent = new Intent(context, IntentActivity.class);

        intent.putExtra(PlayerActivity.VIDEO_ITEM_KEY, (Parcelable) videoItem);
        intent.putExtra(CHOOSE_PLAYER_KEY, true);
        context.startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putAll(mArgs);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mTask != null)
            mTask.cancel(false);
    }

    private LoadVideoItemTask mTask;

    private boolean checkIntent() {
        if (getIntent() == null || getIntent().getData() == null) {
            return false;
        }
        CharSequence intentUrl = getIntent().getData().toString();
        CharSequence intentTitle = null;
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("com.android.extra.filename"))
            intentTitle = getIntent().getExtras().getString("com.android.extra.filename");
        CarambaUrlType carambaUrlType = CarambaApi.getUrlType(intentUrl.toString());
        switch (carambaUrlType) {
            case Video:
                VideoItem videoItem = new VideoItem();
                videoItem.setTitle(intentTitle);
                videoItem.setUrl(intentUrl);
                mTask = new LoadVideoItemTask(this, videoItem);
                mTask.execute();
                return false;
            case Main:
                MainActivity.show(this, 0);
                break;
            case Top:
                MainActivity.show(this, 1);
                break;
            case Channels:
                MainActivity.show(this, 2);
                break;
            case Channel:
                ChannelItem channelItem = new ChannelItem();
                channelItem.setUrl(intentUrl);
                channelItem.setTitle(intentTitle);
                ChannelVideoListActivity.show(this, channelItem);
                return true;
            default:
                return false;
        }
        return false;
    }

    private void startVideo(VideoItem videoItem) {
        VideoInfo videoInfo = getVideoInfo(videoItem);
        try {
            new VideoItemsHistoryContract(getApplicationContext()).put(videoItem);
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
            Uri data = Uri.parse(videoItem.getDefaultVideoUrl().toString());

            if (videoItem.isYoutube() && videoItem.getQualities().size() == 0) {
                intent.setData(data);
            } else {
                intent.setDataAndType(data, "video/*");
                intent.putExtra("com.android.extra.filename", videoItem.getTitle());
                Bundle bundle = new Bundle();
                bundle.putParcelable(PlayerActivity.VIDEO_ITEM_KEY, videoInfo);
                intent.putExtra(PlayerActivity.PROTECTED_BUNDLE_KEY, bundle);
            }

            if (mArgs.containsKey(CHOOSE_PLAYER_KEY))
                startActivity(Intent.createChooser(intent, "Выберите"));
            else
                startActivity(intent);
            finish();
        } catch (Throwable ex) {
            Log.e(IntentActivity.this, ex);
        }
    }

    private VideoInfo getVideoInfo(VideoItem videoItem) {
        VideoInfo videoInfo = new VideoInfo();
        videoInfo.setTitle(videoItem.getTitle());
        videoInfo.setUrl(videoItem.getDefaultVideoUrl());
        for (Quality quality : videoItem.getQualities()) {
            if (videoItem.isYoutube())
                videoInfo.getQualities().add(new VideoInfo(quality.getTitle(), quality.getFileName()));
            else
                videoInfo.getQualities().add(new VideoInfo(quality.getTitle(), videoItem.getFilePath(videoItem.getvId(), quality.getFileName())));
        }
        return videoInfo;
    }


    public class LoadVideoItemTask extends AsyncTask<Boolean, Void, Boolean> {

        private Runnable onCancelAction;
        protected Throwable mEx;

        private VideoItem mVideoItem;
        private Context mContext;

        public LoadVideoItemTask(Context context, VideoItem videoItem) {
            mContext = context;
            mVideoItem = videoItem;
        }

        public void cancel(Runnable runnable) {
            onCancelAction = runnable;

            cancel(false);
        }

        @Override
        protected Boolean doInBackground(Boolean[] p1) {
            try {

                CarambaApi.getVideoInfo(mVideoItem);
                return true;

            } catch (Throwable e) {
                mEx = e;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);

            if (result != null && !isCancelled()) {
                startVideo(mVideoItem);
            }

            if (mEx != null) {
                e(mEx);
            }
        }

        private void e(Throwable ex) {
            android.util.Log.e("LoadVideoItemTask", ex.toString());
            tryShowError(ex);
        }

        private void tryShowError(Throwable ex) {
            if (mContext == null) return;
            try {
                AlertDialog alertDialog =
                        new AlertDialog.Builder(mContext)
                                .setTitle("Ошибка")
                                .setMessage(Log.getMessage(ex))
                                .create();

                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        finish();
                    }
                });
                alertDialog.show();
            } catch (Throwable ignored) {

            }
        }

        @Override
        protected void onCancelled(Boolean result) {
            if (onCancelAction != null)
                onCancelAction.run();
        }

        @Override
        protected void onCancelled() {
            if (onCancelAction != null)
                onCancelAction.run();
        }
    }

}
