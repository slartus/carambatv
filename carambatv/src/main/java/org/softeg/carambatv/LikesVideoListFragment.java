package org.softeg.carambatv;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.softeg.carambatv.db.VideoItemsLikesContract;
import org.softeg.carambatvapi.VideoItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by slartus on 31.05.2014.
 */
public class LikesVideoListFragment extends VideoListFragment {

    public LikesVideoListFragment() {
        super();
    }

    public static final String UNIQUE_NAME = "likes_video";

    @Override
    public String getUniqueName() {
        return UNIQUE_NAME;
    }

    @Override
    protected List<VideoItem> loadData(Boolean mRefresh) throws Exception {
        return new VideoItemsLikesContract(getActivity().getApplicationContext()).read();
    }

    protected List<VideoItem> loadCache() throws IOException, ClassNotFoundException {
        return new ArrayList<>();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        createActionMenu();
    }

    @Override
    protected int getItemResId(){
        return R.layout.short_video_item;
    }

    private void createActionMenu() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MenuFragment mFragment1 = (MenuFragment) fm.findFragmentByTag(MenuFragment.ID);
        if (mFragment1 != null) {
            ft.remove(mFragment1);
        }
        mFragment1 = new MenuFragment();

        ft.add(mFragment1, MenuFragment.ID);
        ft.commit();
    }

    private void clearData() {
        mData.clear();
        getAdapter().notifyDataSetChanged();
    }

    public static final class MenuFragment extends Fragment {
        public static final String ID = "LikesVideoListFragment.MenuFragment";

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setHasOptionsMenu(true);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);

            MenuItem item = menu.add("Очистить лайки")
                    .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            new VideoItemsLikesContract(getActivity()).clear();
                            FragmentManager fm = getFragmentManager();
                            PlaceholderFragment mFragment1 = (PlaceholderFragment) fm.findFragmentById(R.id.container);
                            if (mFragment1 != null&&mFragment1.getUniqueName().equals(LikesVideoListFragment.UNIQUE_NAME))
                                ((LikesVideoListFragment)mFragment1).clearData();
                            return true;
                        }
                    });
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        }
    }


}