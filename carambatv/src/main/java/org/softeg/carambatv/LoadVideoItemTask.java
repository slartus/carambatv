package org.softeg.carambatv;

import android.content.Context;
import android.os.AsyncTask;

import org.softeg.carambatv.common.Log;
import org.softeg.carambatvapi.CarambaApi;
import org.softeg.carambatvapi.VideoItem;

/*
 * Created by Артём on 27.05.2014.
 */
public abstract class LoadVideoItemTask extends AsyncTask<Boolean, Void, Boolean> {

    private Runnable onCancelAction;
    protected Throwable mEx;

    private VideoItem mVideoItem;
    private Context mContext;

    public LoadVideoItemTask(Context context,VideoItem videoItem) {
        mContext = context;
        mVideoItem = videoItem;
    }

    public void cancel(Runnable runnable) {
        onCancelAction = runnable;

        cancel(false);
    }

    @Override
    protected Boolean doInBackground(Boolean[] p1) {
        try {

            CarambaApi.getVideoInfo(mVideoItem);
            return true;

        } catch (Throwable e) {
            mEx = e;
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (result != null && !isCancelled()) {
            itemLoaded(mVideoItem);
        }

        if (mEx != null) {
            Log.e(mContext, mEx);
        }
    }

    protected abstract void itemLoaded(VideoItem videoItem);

    @Override
    protected void onCancelled(Boolean result) {
        if (onCancelAction != null)
            onCancelAction.run();
    }

    @Override
    protected void onCancelled() {
        if (onCancelAction != null)
            onCancelAction.run();
    }
}
