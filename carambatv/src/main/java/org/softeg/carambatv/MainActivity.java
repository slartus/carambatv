package org.softeg.carambatv;


import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.softeg.videoplayer.UriExtensions;
import org.softeg.videoplayer.VideoInfo;


public class MainActivity extends FragmentActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {
    public static final String START_SELECTED_NAVIGATION_ITEM_KEY = "START_SELECTED_NAVIGATION_ITEM_KEY";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private int mSelectNavItem = -1;

    public static void show(Context context, int startSelectedNavigationItem) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(START_SELECTED_NAVIGATION_ITEM_KEY, startSelectedNavigationItem);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent() != null && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(START_SELECTED_NAVIGATION_ITEM_KEY)) {
            mSelectNavItem = getIntent().getExtras().getInt(START_SELECTED_NAVIGATION_ITEM_KEY);
        }

        setContentView(R.layout.activity_main);

        createActionMenu();

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        if (savedInstanceState != null)
            mFragmentStates.putAll(savedInstanceState);
        if (mSelectNavItem != -1)
            mNavigationDrawerFragment.selectItem(mSelectNavItem);
    }

    private void createActionMenu() {
        android.app.FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MenuFragment mFragment1 = (MenuFragment) fm.findFragmentByTag(MenuFragment.ID);
        if (mFragment1 != null) {
            ft.remove(mFragment1);
        }
        mFragment1 = new MenuFragment();

        ft.add(mFragment1, MenuFragment.ID);
        ft.commit();

    }

    private Bundle mFragmentStates = new Bundle();

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        PlaceholderFragment fragment = (PlaceholderFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        NavItems.NavItem navItem = NavItems.getInstance().getItems().get(position);
        if (getActionBar() != null) {
            getActionBar().setTitle(navItem.getTitle());
            getActionBar().setIcon(navItem.getImageId());
        }
        if (fragment != null) {
            if (!navItem.getId().equals(fragment.getUniqueName())) {
                restoreActionBar();
                Bundle args = new Bundle();
                fragment.onSaveInstanceState(args);
                mFragmentStates.putBundle(fragment.getUniqueName(), args);
                fragment = null;
            }
        }
        if (fragment == null) {
            fragment = navItem.createFragment();
            fragment.setArguments(mFragmentStates.getBundle(fragment.getUniqueName()));
        }
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        PlaceholderFragment fragment = (PlaceholderFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        Bundle args = new Bundle();
        fragment.onSaveInstanceState(args);
        mFragmentStates.putBundle(fragment.getUniqueName(), args);
        outState.putAll(mFragmentStates);
        super.onSaveInstanceState(outState);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    public static final class MenuFragment extends Fragment {
        public static final String ID = "MainActivity.MenuFragment";

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setHasOptionsMenu(true);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);

            MenuItem item = menu.add("Обсудить программу")
                    .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            UriExtensions.showInBrowser(getActivity(), "http://4pda.ru/forum/index.php?showtopic=575622");
                            return true;
                        }
                    });
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

            item = menu.add("Donate")
                    .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            UriExtensions.showInBrowser(getActivity(), "http://softeg.org/donate");
                            return true;
                        }
                    });
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        }
    }
}
