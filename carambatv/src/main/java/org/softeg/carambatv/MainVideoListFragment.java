package org.softeg.carambatv;

import org.softeg.carambatvapi.CarambaApi;
import org.softeg.carambatvapi.VideoItem;

import java.util.List;

/*
 * Created by Артём on 26.05.2014.
 */
public class MainVideoListFragment extends VideoListFragment {
    public MainVideoListFragment() {
        super();
    }

    public static final String UNIQUE_NAME = "main";

    @Override
    protected List<VideoItem> loadData(Boolean mRefresh) throws Exception {
        mListInfo.setFrom(mRefresh ? 0 : mData.size());
        if (mRefresh)
            mListInfo.setMax(0);
        return CarambaApi.getMain(App.getInstance(),mListInfo);
    }

    @Override
    public String getUniqueName() {
        return UNIQUE_NAME;
    }
}
