package org.softeg.carambatv;

import java.util.ArrayList;

/*
 * Created by Артём on 26.05.2014.
 */
public class NavItems {
    private static NavItems mInstance=null;

    private ArrayList<NavItem> mItems=null;
    public static NavItems getInstance(){
        if(mInstance==null){
            mInstance=new NavItems();
        }
        return mInstance;
    }
    private NavItems(){
        mItems=new ArrayList<>();
        mItems.add(new NavItem() {
            @Override
            String getId() {
                return MainVideoListFragment.UNIQUE_NAME;
            }

            @Override
            int getTitle() {
                return R.string.main;
            }

            @Override
            int getImageId() {
                return R.drawable.main;
            }

            @Override
            PlaceholderFragment createFragment() {
                return new  MainVideoListFragment();
            }
        });
//        mItems.add(new NavItem() {
//            @Override
//            String getId() {
//                return TopVideoListFragment.UNIQUE_NAME;
//            }
//
//            @Override
//            int getTitle() {
//                return R.string.top;
//            }
//
//            @Override
//            int getImageId() {
//                return R.drawable.top;
//            }
//
//            @Override
//            PlaceholderFragment createFragment() {
//                return new TopVideoListFragment();
//            }
//        });
        mItems.add(new NavItem() {
            @Override
            String getId() {
                return ChannelsListFragment.UNIQUE_NAME;
            }

            @Override
            int getTitle() {
                return R.string.show;
            }

            @Override
            int getImageId() {
                return R.drawable.shows;
            }

            @Override
            PlaceholderFragment createFragment() {
                return new ChannelsListFragment();
            }
        });
        mItems.add(new NavItem() {
            @Override
            String getId() {
                return HistoryVideoListFragment.UNIQUE_NAME;
            }

            @Override
            int getTitle() {
                return R.string.hitosry_views;
            }

            @Override
            int getImageId() {
                return R.drawable.history;
            }

            @Override
            PlaceholderFragment createFragment() {
                return new HistoryVideoListFragment();
            }
        });
        mItems.add(new NavItem() {
            @Override
            String getId() {
                return LikesVideoListFragment.UNIQUE_NAME;
            }

            @Override
            int getTitle() {
                return R.string.likes_views;
            }

            @Override
            int getImageId() {
                return R.drawable.likes;
            }

            @Override
            PlaceholderFragment createFragment() {
                return new LikesVideoListFragment();
            }
        });
    }

    public ArrayList<NavItem> getItems(){
        return mItems;
    }



    public abstract class NavItem{
        abstract String getId();
        abstract int getTitle();
        abstract int getImageId();
        abstract PlaceholderFragment createFragment();
    }
}
