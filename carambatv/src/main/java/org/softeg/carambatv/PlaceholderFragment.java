package org.softeg.carambatv;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * A placeholder fragment containing a simple view.
 */
public abstract class PlaceholderFragment extends Fragment {
    public PlaceholderFragment() {
    }


    public abstract String getUniqueName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    protected abstract void reloadData();

}