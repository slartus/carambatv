package org.softeg.carambatv;

import org.softeg.carambatvapi.CarambaApi;
import org.softeg.carambatvapi.VideoItem;

import java.util.List;

/*
 * Created by Артём on 26.05.2014.
 */
public class TopVideoListFragment extends VideoListFragment {
    public  TopVideoListFragment(){
        super();
    }
    public static final String UNIQUE_NAME="top";
    @Override
    public String getUniqueName() {
        return UNIQUE_NAME;
    }

    @Override
    protected List<VideoItem> loadData(Boolean mRefresh) throws Exception {
        return CarambaApi.getTop();
    }

    @Override
    protected int getItemResId(){

        return R.layout.top_video_item;
    }
}
