package org.softeg.carambatv;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.softeg.carambatv.common.Log;

import org.softeg.carambatv.db.VideoItemsLikesContract;
import org.softeg.carambatvapi.ListInfo;
import org.softeg.carambatvapi.VideoItem;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;


import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.Options;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;


/*
 * Created by Артём on 25.05.2014.
 */
public abstract class VideoListFragment extends PlaceholderFragment implements ListView.OnItemClickListener {

    private GridView mListView;
    private ListAdapter mAdapter;
    private View btnUp;
    protected ArrayList<VideoItem> mData = new ArrayList<>();
    public static final String LIST_INFO_KEY = "LIST_INFO_KEY";
    public static final String LIST_VIEW_DATA_KEY = "LIST_VIEW_DATA_KEY";
    public static final String LIST_VIEW_POSITION_KEY = "LIST_VIEW_POSITION_KEY";


    private Task mTask = null;
    private LoadCacheTask mCacheTask = null;
    protected ListInfo mListInfo = new ListInfo();
    protected PullToRefreshLayout mPullToRefreshLayout;

    @Override
    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.video_list_fragment, container, false);
        assert v != null;
        mListView = (GridView) v.findViewById(R.id.gridView1);
        mListView.setOnItemClickListener(this);
        mListView.setEmptyView(v.findViewById(android.R.id.empty));
        btnUp = v.findViewById(R.id.btnup);
        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListView.smoothScrollToPosition(0);
            }
        });
        return v;
    }

    public VideoListFragment.ListAdapter getAdapter() {
        return mAdapter;
    }

    protected int getItemResId() {
        return R.layout.video_item;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        registerForContextMenu(mListView);

        mAdapter = new ListAdapter(getActivity(), mData, getItemResId());

        mListView.setAdapter(mAdapter);

        Bundle args = new Bundle();
        if (getArguments() != null)
            args.putAll(getArguments());
        if (savedInstanceState != null)
            args.putAll(savedInstanceState);

        if (args.containsKey(LIST_INFO_KEY))
            mListInfo = args.getParcelable(LIST_INFO_KEY);


        if (args.containsKey(LIST_VIEW_DATA_KEY)) {
            ArrayList<VideoItem> data = args.getParcelableArrayList(LIST_VIEW_DATA_KEY);
            if (data != null)
                mData.addAll(data);
            mAdapter.notifyDataSetChanged();

            mListView.smoothScrollToPosition(args.getInt(LIST_VIEW_POSITION_KEY, 0));
        } else {
            mCacheTask = new LoadCacheTask();
            mCacheTask.execute();
        }

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem > 2) {
                    btnUp.setVisibility(View.VISIBLE);
                    mHideBtnUpHandler.removeCallbacks(mHideBtnUpRunnable);
                    mHideBtnUpHandler.postDelayed(mHideBtnUpRunnable, 2000);
                } else {
                    btnUp.setVisibility(View.GONE);
                }

                final int lastItem = firstVisibleItem + visibleItemCount;
                if (lastItem == totalItemCount) {
                    if (mData.size() >= mListInfo.getMax()) return;
                    if (mTask == null || mTask.getStatus() == AsyncTask.Status.FINISHED)
                        loadData(false);
                }
            }
        });
    }

    Handler mHideBtnUpHandler = new Handler();
    Runnable mHideBtnUpRunnable = new Runnable() {
        @Override
        public void run() {
            btnUp.setVisibility(View.GONE);
        }
    };

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPullToRefreshLayout = createPullToRefreshLayout(view);
    }

    protected PullToRefreshLayout createPullToRefreshLayout(View view) {
        // We need to create a PullToRefreshLayout manually
        PullToRefreshLayout pullToRefreshLayout = (PullToRefreshLayout) view.findViewById(R.id.ptr_layout);
        assert pullToRefreshLayout != null;
        // We can now setup the PullToRefreshLayout
        ActionBarPullToRefresh.from(getActivity())
                .options(Options.create().scrollDistance(0.3f).refreshOnUp(true).build())
                // We need to insert the PullToRefreshLayout into the Fragment's ViewGroup
                .allChildrenArePullable()

                // We can now complete the setup as desired
                .listener(new OnRefreshListener() {
                    @Override
                    public void onRefreshStarted(View view) {
                        loadData(true);
                    }
                })
                .setup(pullToRefreshLayout);
        return pullToRefreshLayout;
    }

    @Override
    public void onSaveInstanceState(Bundle args) {
        if (mData.size() > 0)
            args.putParcelableArrayList(LIST_VIEW_DATA_KEY, mData);
        args.putParcelable(LIST_INFO_KEY, mListInfo);
        args.putInt(LIST_VIEW_POSITION_KEY, mListView.getFirstVisiblePosition());
        super.onSaveInstanceState(args);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        setLoading(false);

        if (mCacheTask != null)
            mCacheTask.cancel(false);
        if (mTask != null)
            mTask.cancel(null);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        try {

            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            if (info.id == -1) return;
            Object o = mAdapter.getItem((int) info.id);
            if (o == null)
                return;
            final VideoItem item = (VideoItem) o;

            menu.add("Воспроизвести").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    IntentActivity.show(getActivity(), item);
                    return true;
                }
            });
            menu.add("Воспроизвести в").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    IntentActivity.showChooser(getActivity(), item);
                    return true;
                }
            });
            if (!getUniqueName().equals(LikesVideoListFragment.UNIQUE_NAME)) {
                menu.add("В избранные ролики").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        try {
                            new VideoItemsLikesContract(getActivity().getApplicationContext()).put(item);
                            Toast.makeText(getActivity(), "Добавлен в избранное", Toast.LENGTH_SHORT).show();
                        } catch (Throwable ex) {
                            Log.e(getActivity(), ex);
                        }
                        return true;
                    }
                });
            } else {
                menu.add("Убрать из избранного").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        try {
                            new VideoItemsLikesContract(getActivity().getApplicationContext()).delete(item);
                            Toast.makeText(getActivity(), "Удалён из избранного", Toast.LENGTH_SHORT).show();
                        } catch (Throwable ex) {
                            Log.e(getActivity(), ex);
                        }
                        return true;
                    }
                });
            }
            menu.add("Ссылка на ролик")
                    .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            org.softeg.carambatvapi.common.UriExtensions.showSelectActionDialog(getActivity(), item.getUrl().toString());
                            return true;
                        }
                    });
//            menu.add("Ссылка на ролик..").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                @Override
//                public boolean onMenuItemClick(MenuItem menuItem) {
//                    IntentActivity.showChooser(getActivity(), item);
//                    return true;
//                }
//            });

        } catch (Throwable ex) {
            Log.e(getActivity(), ex);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        final VideoItem item = (VideoItem) mAdapter.getItem((int) l);

        IntentActivity.show(getActivity(), item);

    }

    protected void deliveryResult(List<VideoItem> result, Boolean mRefresh) {
        if (mRefresh)
            mData.clear();
        mData.addAll(result);
        mAdapter.notifyDataSetChanged();
        try {
            saveCache(mData);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected abstract List<VideoItem> loadData(Boolean mRefresh) throws Exception;

    private void setLoading(boolean loading) {
        if (getActivity() == null || mPullToRefreshLayout == null) return;

        mPullToRefreshLayout.setRefreshing(loading);
    }

    private void startLoad() {
        if (mTask != null)
            return;
        loadData(true);
    }

    @Override
    protected void reloadData() {
        loadData(true);
    }

    public void loadData(final boolean isRefresh) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                mTask = new Task(isRefresh);
                mTask.execute();
            }
        };
        if (mTask != null && mTask.getStatus() != AsyncTask.Status.FINISHED)
            mTask.cancel(runnable);
        else {
            runnable.run();
        }
    }


    public class ListAdapter extends BaseAdapter {
        private final LayoutInflater mInflater;
        protected ArrayList<VideoItem> mData;
        private Context mContext;
        private int resId;

        public ListAdapter(Context context, ArrayList<VideoItem> data, int resId) {
            this.resId = resId;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mData = data;
            mContext = context;

        }

        @Override
        public int getCount() {
            return mData == null ? 0 : mData.size();
        }

        @Override
        public Object getItem(int i) {
            return mData.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public android.view.View getView(int position, android.view.View view, android.view.ViewGroup parent) {
            final ViewHolder holder;
            if (view == null) {
                view = mInflater.inflate(resId, parent, false);
                holder = new ViewHolder();
                assert view != null;
                holder.image = (ImageView) view.findViewById(R.id.imageView);
                holder.progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
                holder.imageError = view.findViewById(R.id.image_error);
                holder.channelTitleTextView = (TextView) view.findViewById(R.id.channelTitleTextView);
                holder.dateTextView = (TextView) view.findViewById(R.id.date_text);
                holder.titleTextView = (TextView) view.findViewById(R.id.titleTextView);
                holder.likesTextView = (TextView) view.findViewById(R.id.likes_text);
                holder.viewsTextView = (TextView) view.findViewById(R.id.views_text);
                view.setTag(holder);

            } else {
                holder = (ViewHolder) view.getTag();
                if (holder.progressBar != null)
                    holder.progressBar.setVisibility(View.VISIBLE);
                if (holder.imageError != null)
                    holder.imageError.setVisibility(View.GONE);
            }
            VideoItem item = (VideoItem) getItem(position);

            if (item.getPreviewImgUrl() == null) {
                if (holder.progressBar != null)
                    holder.progressBar.setVisibility(View.GONE);
            } else {
                try {
                    Picasso.with(mContext).load(item.getPreviewImgUrl().toString())
                            .into(holder.image, new Callback() {
                                @Override
                                public void onSuccess() {
                                    holder.progressBar.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    holder.progressBar.setVisibility(View.GONE);
                                    holder.imageError.setVisibility(View.VISIBLE);
                                }
                            });
                } catch (Throwable ex) {
                    ex.printStackTrace();
                    holder.progressBar.setVisibility(View.GONE);
                    holder.imageError.setVisibility(View.VISIBLE);
                }

            }
            if (holder.dateTextView != null)
                holder.dateTextView.setText(item.getDateString());
            if (holder.channelTitleTextView != null)
                holder.channelTitleTextView.setText(item.getChannelTitle());
            if (holder.titleTextView != null)
                holder.titleTextView.setText(item.getTitle());
            if (holder.likesTextView != null)
                holder.likesTextView.setText(Integer.toString(item.getVideoSummary().getLikesCount()));
            if (holder.viewsTextView != null)
                holder.viewsTextView.setText(Integer.toString(item.getVideoSummary().getViewsCount()));
            return view;
        }

        class ViewHolder {
            ImageView image;
            ProgressBar progressBar;
            View imageError;
            TextView channelTitleTextView;
            TextView dateTextView;
            TextView titleTextView;
            TextView likesTextView;
            TextView viewsTextView;
        }
    }

    public class Task extends AsyncTask<Boolean, Void, List<VideoItem>> {
        protected Boolean mRefresh;
        private Runnable onCancelAction;
        protected Throwable mEx;

        public Task(Boolean refresh) {
            mRefresh = refresh;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            setLoading(true);
        }

        public void cancel(Runnable runnable) {
            onCancelAction = runnable;

            cancel(false);
        }

        @Override
        protected List<VideoItem> doInBackground(Boolean[] p1) {
            try {

                return loadData(mRefresh);

            } catch (Throwable e) {
                mEx = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<VideoItem> result) {
            super.onPostExecute(result);
            if (result != null && !isCancelled()) {
                deliveryResult(result, mRefresh);
            }
            setLoading(false);
            if (mEx != null) {
                Log.e(getActivity(), mEx);
            }
        }

        @Override
        protected void onCancelled(List<VideoItem> result) {
            if (onCancelAction != null)
                onCancelAction.run();
        }

        @Override
        protected void onCancelled() {
            if (onCancelAction != null)
                onCancelAction.run();
        }
    }

    public class LoadCacheTask extends AsyncTask<Boolean, Void, List<VideoItem>> {
        private Throwable mEx;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setLoading(true);
        }

        @Override
        protected List<VideoItem> doInBackground(Boolean[] p1) {
            try {

                return loadCache();

            } catch (Throwable e) {
                mEx = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<VideoItem> result) {
            super.onPostExecute(result);

            if (mEx != null) {
                mEx.printStackTrace();
            }

            if (!isCancelled()) {
                deliveryCache(result);

                startLoad();
            }
        }

        @Override
        protected void onCancelled(List<VideoItem> result) {
            setLoading(false);
        }

        @Override
        protected void onCancelled() {
            setLoading(false);
        }

    }

    protected List<VideoItem> loadCache() throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = null;
        FileInputStream fileInputStream = null;
        try {

            fileInputStream = getActivity().openFileInput(getUniqueName() + ".tmp");
            objectInputStream = new ObjectInputStream(fileInputStream);
            return (ArrayList<VideoItem>) objectInputStream.readObject();
        } finally {
            if (objectInputStream != null)
                objectInputStream.close();
            if (fileInputStream != null)
                fileInputStream.close();
        }
    }

    protected void saveCache(ArrayList<VideoItem> items) throws IOException, ClassNotFoundException {
        if (items == null || items.size() == 0)
            return;
        ObjectOutputStream objectOutputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = getActivity().openFileOutput(getUniqueName() + ".tmp", Context.MODE_PRIVATE);
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(items);
            objectOutputStream.close();
        } finally {
            if (objectOutputStream != null)
                objectOutputStream.close();
            if (fileOutputStream != null)
                fileOutputStream.close();
        }
    }

    protected void deliveryCache(List<VideoItem> cacheList) {
        mData.clear();
        if (cacheList != null) {
            mData.addAll(cacheList);
        }

        mAdapter.notifyDataSetChanged();
    }
}
