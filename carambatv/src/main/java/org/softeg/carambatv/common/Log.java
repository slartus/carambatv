package org.softeg.carambatv.common;

import android.app.AlertDialog;
import android.content.Context;
import android.text.TextUtils;

import org.softeg.carambatv.AppLog;

/**
 * Created by Артём on 26.05.2014.
 */
public class Log {

    private static final String TAG = "org.softeg.aycaramba.common.Log";

    public static void e(Context context, Throwable ex) {
        AppLog.e(context,ex);
    }

    private static void tryShowError(Context context, Throwable ex) {
        if (context == null) return;
        try {
            new AlertDialog.Builder(context)
                    .setTitle("Ошибка")
                    .setMessage(getMessage(ex))
                    .create().show();
        } catch (Throwable ignored) {

        }
    }

    public static String getMessage(Throwable ex){

        String message = ex.getMessage();
        if (TextUtils.isEmpty(message))
            message = ex.toString();
        return message;
    }
}
