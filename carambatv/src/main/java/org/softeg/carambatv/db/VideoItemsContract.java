package org.softeg.carambatv.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import org.softeg.carambatvapi.VideoItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/*
 * Created by slartus on 31.05.2014.
 */
public abstract class VideoItemsContract {
    private Context mContext;

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public VideoItemsContract(Context context) {

        mContext = context;
    }

    protected  abstract String getTableName();

    /* Inner class that defines the table contents */
    public static abstract class VideoItemsTable implements BaseColumns {

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_VID = "vid";
        public static final String COLUMN_PREVIEWIMGURL = "previewimgurl";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_URL = "url";
        public static final String COLUMN_CHANNEL_URL = "channelurl";
        public static final String COLUMN_DATE_STR = "datestr";
        public static final String COLUMN_CHANNEL_TITLE = "channeltitle";
        public static final String COLUMN_LIKES_COUNT = "likes_count";
        public static final String COLUMN_VIEWS_COUNT = "views_count";
        public static final String COLUMN_DATE_VIEW = "dateview";
    }

    public static String getValueOrEmpty(CharSequence str) {
        if (str == null)
            return null;
        return str.toString();
    }

    private static SimpleDateFormat carambaDateFormat = new SimpleDateFormat("dd MMM");
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

    private final int MAX_ROWS_COUNT = 40;

    protected void deleteLimits(SQLiteDatabase db) {
        db.execSQL("DELETE FROM " + getTableName() + " where _id NOT IN (SELECT _id from " + getTableName() + " ORDER BY dateview DESC LIMIT " + MAX_ROWS_COUNT + ")");

    }

    public void clear() {
        SQLiteDatabase db = null;
        CarambaDbHelper mDbHelper = new CarambaDbHelper(mContext);
        try {
            db = mDbHelper.getWritableDatabase();
            db.execSQL("DELETE FROM "+getTableName());
        } finally {
            if (db != null)
                db.close();
        }
    }

    public void delete(VideoItem videoItem) {
        CarambaDbHelper mDbHelper = new CarambaDbHelper(mContext);
        SQLiteDatabase db = null;

        try {
            db = mDbHelper.getWritableDatabase();
            db.execSQL("delete from " + getTableName() + " where " + VideoItemsTable.COLUMN_ID + "=" + videoItem.getId());
        } finally {
            if (db != null)
                db.close();
        }
    }

    public void put(VideoItem videoItem) {
        CarambaDbHelper mDbHelper = new CarambaDbHelper(mContext);
        SQLiteDatabase db = null;

        try {
            db = mDbHelper.getWritableDatabase();
            db.execSQL("delete from " + getTableName() + " where " + VideoItemsTable.COLUMN_ID + "=" + videoItem.getId());
            Calendar today = new GregorianCalendar();
            today.setTime(new Date());

            ContentValues values = new ContentValues();
            values.put(VideoItemsTable.COLUMN_ID, getValueOrEmpty(videoItem.getId()));
            values.put(VideoItemsTable.COLUMN_VID, getValueOrEmpty(videoItem.getvId()));
            values.put(VideoItemsTable.COLUMN_PREVIEWIMGURL, getValueOrEmpty(videoItem.getPreviewImgUrl()));
            values.put(VideoItemsTable.COLUMN_TITLE, getValueOrEmpty(videoItem.getTitle()));
            values.put(VideoItemsTable.COLUMN_URL, getValueOrEmpty(videoItem.getUrl()));
            values.put(VideoItemsTable.COLUMN_CHANNEL_URL, getValueOrEmpty(videoItem.getChannelUrl()));
            String dateStr = getValueOrEmpty(videoItem.getDateString());
            if (dateStr != null) {
                Calendar yesterday = new GregorianCalendar();
                yesterday.setTime(new Date());
                yesterday.add(Calendar.DAY_OF_YEAR, -1);
                String todayStr = carambaDateFormat.format(today.getTime());
                String yesterdayStr = carambaDateFormat.format(yesterday.getTime());

                dateStr = dateStr.replace("Вчера", yesterdayStr).replace("Сегодня", todayStr);
            }
            values.put(VideoItemsTable.COLUMN_DATE_STR, dateStr);
            values.put(VideoItemsTable.COLUMN_CHANNEL_TITLE, getValueOrEmpty(videoItem.getChannelTitle()));
            values.put(VideoItemsTable.COLUMN_DATE_VIEW, dateFormat.format(today.getTime()));
            values.put(VideoItemsTable.COLUMN_LIKES_COUNT, videoItem.getVideoSummary().getLikesCount());
            values.put(VideoItemsTable.COLUMN_VIEWS_COUNT, videoItem.getVideoSummary().getViewsCount());
            db.insertOrThrow(getTableName(), null, values);
            deleteLimits(db);
        } finally {
            if (db != null)
                db.close();
        }
    }

    public List<VideoItem> read() {

        CarambaDbHelper mDbHelper = new CarambaDbHelper(mContext);
        SQLiteDatabase db = null;
        Cursor c = null;
        ArrayList<VideoItem> res = new ArrayList<>();
        try {
            db = mDbHelper.getWritableDatabase();
            c = db.query(getTableName(), null, null, null, null, null, VideoItemsTable.COLUMN_DATE_VIEW + " DESC");
            while (c.moveToNext()) {
                VideoItem item = new VideoItem();
                item.setId(c.getString(c.getColumnIndex(VideoItemsTable.COLUMN_ID)));
                item.setvId(c.getString(c.getColumnIndex(VideoItemsTable.COLUMN_VID)));
                item.setPreviewImgUrl(c.getString(c.getColumnIndex(VideoItemsTable.COLUMN_PREVIEWIMGURL)));
                item.setTitle(c.getString(c.getColumnIndex(VideoItemsTable.COLUMN_TITLE)));
                item.setUrl(c.getString(c.getColumnIndex(VideoItemsTable.COLUMN_URL)));
                item.setChannelUrl(c.getString(c.getColumnIndex(VideoItemsTable.COLUMN_CHANNEL_URL)));
                item.setDateString(c.getString(c.getColumnIndex(VideoItemsTable.COLUMN_DATE_STR)));
                item.setChannelTitle(c.getString(c.getColumnIndex(VideoItemsTable.COLUMN_CHANNEL_TITLE)));
                item.getVideoSummary().setLikesCount(c.getInt(c.getColumnIndex(VideoItemsTable.COLUMN_LIKES_COUNT)));
                item.getVideoSummary().setViewsCount(c.getInt(c.getColumnIndex(VideoItemsTable.COLUMN_VIEWS_COUNT)));
                res.add(item);
            }
        } finally {
            if (c != null)
                c.close();
            if (db != null)
                db.close();
        }
        return res;
    }
}