package org.softeg.carambatv.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.softeg.carambatvapi.VideoItem;

/**
 * Created by slartus on 31.05.2014.
 */
public class VideoItemsLikesContract extends VideoItemsContract {
    public static final String TABLE_NAME = "VideoItemsLikes";
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    VideoItemsContract.VideoItemsTable._ID + " INTEGER PRIMARY KEY," +
                    VideoItemsContract.VideoItemsTable.COLUMN_ID + TEXT_TYPE + COMMA_SEP +
                    VideoItemsContract.VideoItemsTable.COLUMN_VID + TEXT_TYPE + COMMA_SEP +
                    VideoItemsContract.VideoItemsTable.COLUMN_PREVIEWIMGURL + TEXT_TYPE + COMMA_SEP +
                    VideoItemsContract.VideoItemsTable.COLUMN_TITLE + TEXT_TYPE + COMMA_SEP +
                    VideoItemsContract.VideoItemsTable.COLUMN_URL + TEXT_TYPE + COMMA_SEP +
                    VideoItemsContract.VideoItemsTable.COLUMN_CHANNEL_URL + TEXT_TYPE + COMMA_SEP +
                    VideoItemsContract.VideoItemsTable.COLUMN_DATE_STR + TEXT_TYPE + COMMA_SEP +
                    VideoItemsContract.VideoItemsTable.COLUMN_DATE_VIEW + TEXT_TYPE + COMMA_SEP +
                    VideoItemsContract.VideoItemsTable.COLUMN_LIKES_COUNT + INTEGER_TYPE + COMMA_SEP +
                    VideoItemsContract.VideoItemsTable.COLUMN_VIEWS_COUNT + INTEGER_TYPE + COMMA_SEP +
                    VideoItemsContract.VideoItemsTable.COLUMN_CHANNEL_TITLE + TEXT_TYPE +
                    " )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    public VideoItemsLikesContract(Context context) {
        super(context);
    }

    @Override
    protected void deleteLimits(SQLiteDatabase db) {
    }

    protected String getTableName() {
        return TABLE_NAME;
    }


}
