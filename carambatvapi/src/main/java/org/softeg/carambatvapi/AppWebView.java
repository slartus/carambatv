package org.softeg.carambatvapi;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by slinkin on 13.04.2015.
 */
public class AppWebView {
    public void parse(Context context,String url, OnHtmlLoadedListener onHtmlLoadedAction){
        final WebView webView=new WebView(context);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.addJavascriptInterface(new MyJavaScriptInterface(onHtmlLoadedAction), "HtmlViewer");

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                webView.loadUrl("javascript:window.HtmlViewer.showHTML" +
                        "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }
        });

        webView.loadUrl(url);
    }

    class MyJavaScriptInterface {


        private OnHtmlLoadedListener onHtmlLoadedAction;

        MyJavaScriptInterface(OnHtmlLoadedListener onHtmlLoadedAction) {
            this.onHtmlLoadedAction = onHtmlLoadedAction;
        }

        @JavascriptInterface
        public void showHTML(String html) {

            onHtmlLoadedAction.loaded(html);
        }

    }

    public interface OnHtmlLoadedListener{
        void loaded(String html);
    }
}
