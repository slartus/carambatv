package org.softeg.carambatvapi;

import android.content.Context;
import android.net.Uri;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.softeg.carambatvapi.common.Http;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by Артём on 25.05.2014.
 */
public class CarambaApi {


    public static List<VideoItem> getTop() throws Exception {
        String page = Http.getPage("http://carambatv.ru", "UTF-8");
        Document doc = Jsoup.parse(page);
        Elements divItems = doc.select("a.search_block_shows__results_item");
        List<VideoItem> res = new ArrayList<>();
        for (Element itemElement : divItems) {
            VideoItem item = new VideoItem();
            Uri uri=Uri.parse(itemElement.attr("href"));
            item.setId(uri.getPathSegments().get(0));
            item.setChannelUrl(uri.toString());
            item.setChannelTitle(itemElement.text());
            res.add(item);
        }
        return res;
    }

    public static void getVideoInfo(VideoItem videoItem) throws Exception {
        VideoItemApi.getVideoInfo(videoItem);
    }

    public static List<ChannelItem> getChannels() throws Exception {
        return ChannelsApi.getChannels();
    }

    public static List<VideoItem> getMain(Context context, ListInfo listInfo) throws Exception {
        List<VideoItem> res;
        if (listInfo.getFrom() == 0)
            res = VideoListApi.parseFromUrl("http://carambatv.ru/");
        else {
            List<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair("limit", "36"));
            nvps.add(new BasicNameValuePair("from", Integer.toString(listInfo.getFrom())));
            nvps.add(new BasicNameValuePair("action", "crmb_load_videos"));
            String page = Http.httpPost("http://carambatv.ru/wp-admin/admin-ajax.php", nvps, "UTF-8");
            JSONObject jsonObject = new JSONObject(page);
            if (!jsonObject.getBoolean("success")) {
                listInfo.setMax(listInfo.getFrom());
                return new ArrayList<>();
            }
            Document doc = Jsoup.parse(jsonObject.getJSONObject("data").getString("html"));
            res = VideoListApi.parse(doc.select("div.item"));
        }
        if (res.size() == 0) {
            listInfo.setMax(listInfo.getFrom());
        } else {
            listInfo.setMax(listInfo.getFrom() + res.size() + 1);
        }
        return res;
    }

    public static List<VideoItem> getChannelVideoList(ChannelItem channelItem, ListInfo listInfo) throws Exception {
        return ChannelsApi.getChannelVideoList(channelItem, listInfo);
    }

    public static CarambaUrlType getUrlType(String url) {
        if (VideoItemApi.isVideoUrl(url))
            return CarambaUrlType.Video;

        Uri uri = Uri.parse(url.toLowerCase());

        if (uri.getPathSegments().size()==0)
            return CarambaUrlType.Main;
        switch (uri.getPathSegments().get(0)){
            case "top":
                return CarambaUrlType.Top;
            case "channels":
                return CarambaUrlType.Channels;
            case "advert":
            case "contacts":
                return CarambaUrlType.Unknown;
        }

        if (uri.getPathSegments().size()==2)
            return CarambaUrlType.Channel;

        return CarambaUrlType.Unknown;
    }
}
