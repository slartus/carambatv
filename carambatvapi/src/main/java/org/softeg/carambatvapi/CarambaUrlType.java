package org.softeg.carambatvapi;

/**
 * Created by Артём on 27.05.2014.
 */
public enum CarambaUrlType {
    Unknown,
    Main,
    Top,
    Channels,
    Channel,
    Video
}
