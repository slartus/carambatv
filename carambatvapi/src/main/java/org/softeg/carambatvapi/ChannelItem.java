package org.softeg.carambatvapi;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Артём on 26.05.2014.
 */
public class ChannelItem implements Parcelable, Serializable {
    private CharSequence id;

    private CharSequence url;
    private CharSequence title;
    private CharSequence tagTitle;
    private CharSequence imageUrl;

    public ChannelItem() {

    }

    public CharSequence getId() {
        return id;
    }

    public void setId(CharSequence id) {
        this.id = id;
    }

    public CharSequence getUrl() {
        return url;
    }

    public CharSequence getTitleFromUrl() {
        Uri uri = Uri.parse(url.toString());
        return uri.getPathSegments().get(1);
    }

    public void setUrl(CharSequence url) {
        this.url = url;
    }

    public CharSequence getTitle() {
        return title;
    }

    public void setTitle(CharSequence title) {
        this.title = title;
    }

    public CharSequence getTagTitle() {
        return tagTitle;
    }

    public void setTagTitle(CharSequence tagTitle) {
        this.tagTitle = tagTitle;
    }

    public CharSequence getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(CharSequence imageUrl) {
        this.imageUrl = imageUrl;
    }

    public static final Creator<ChannelItem> CREATOR
            = new Creator<ChannelItem>() {
        public ChannelItem createFromParcel(Parcel in) {
            return new ChannelItem(in);
        }

        public ChannelItem[] newArray(int size) {
            return new ChannelItem[size];
        }
    };

    protected ChannelItem(Parcel parcel) {
        id = parcel.readString();
        url = parcel.readString();
        title = parcel.readString();
        tagTitle = parcel.readString();
        imageUrl = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(VideoItem.getValueOrEmpty(id));
        parcel.writeString(VideoItem.getValueOrEmpty(url));
        parcel.writeString(VideoItem.getValueOrEmpty(title));
        parcel.writeString(VideoItem.getValueOrEmpty(tagTitle));
        parcel.writeString(VideoItem.getValueOrEmpty(imageUrl));
    }


}
