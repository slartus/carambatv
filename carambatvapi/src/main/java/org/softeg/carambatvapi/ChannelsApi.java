package org.softeg.carambatvapi;

import android.text.Html;
import android.text.TextUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.softeg.carambatvapi.common.Http;
import org.softeg.carambatvapi.common.IntegerExtensions;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Артём on 26.05.2014.
 */
public class ChannelsApi {

    public static List<VideoItem> getChannelVideoListFromUrl(ChannelItem channelItem) throws Exception {
        String page=Http.getPage(channelItem.getUrl(),"UTF-8");
        Matcher m= Pattern.compile("data-action=\"crmb_toggle_subscription\"\\s*data-show_id=\"(\\d+)\"",Pattern.CASE_INSENSITIVE).matcher(page);
        if(m.find()){
            channelItem.setId(m.group(1));
            m= Pattern.compile("data-show_id=\""+channelItem.getId()+"\"><i class=\"icon-check\"></i></span>\\s*<a class=\"box-name\" href=\"([^\"]*)\">(.*?)</a>",
                    Pattern.CASE_INSENSITIVE).matcher(page);
            if(m.find()) {
                channelItem.setUrl(m.group(1));
                channelItem.setTitle(Html.fromHtml(m.group(2)).toString());
            }
        }


        Document doc = Jsoup.parse(page);

        List<VideoItem> res=  VideoListApi.parse(doc.select("div.items>div.item"));


        return res;
    }

    public static List<VideoItem> getChannelVideoList(ChannelItem channelItem, ListInfo listInfo) throws Exception {
        if(channelItem.getId()==null)
            return getChannelVideoListFromUrl(channelItem);
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("limit", "24"));
        nvps.add(new BasicNameValuePair("from", Integer.toString(listInfo.getFrom())));
        nvps.add(new BasicNameValuePair("action", "crmb_load_videos_by_show"));
        nvps.add(new BasicNameValuePair("term_id", channelItem.getId().toString()));
        String page = Http.httpPost("http://carambatv.ru/wp-admin/admin-ajax.php", nvps, "UTF-8");
        JSONObject jsonObject = new JSONObject(page);
        if (!jsonObject.getBoolean("success")) {
            listInfo.setMax(listInfo.getFrom());
            return new ArrayList<>();
        }
        listInfo.setMax(jsonObject.getJSONObject("data").getInt("total"));

        Document doc = Jsoup.parse(jsonObject.getJSONObject("data").getString("html"));

        List<VideoItem> res=  VideoListApi.parse(doc.select("div.item"));


        return res;
    }

    public static List<ChannelItem> getChannels() throws Exception {
        String page = Http.getPage("http://carambatv.ru/channels/", "UTF-8");
        Document doc = Jsoup.parse(page);
        Elements divItems = doc.select("div.items-shows>div.item");
        List<ChannelItem> res = new ArrayList<>();
        for (Element itemElement : divItems) {
            ChannelItem item = new ChannelItem();


            Elements elements = itemElement.select("a.item-a");
            if (elements.size() > 0) {
                Element itemAElement = elements.first();
                item.setUrl(itemAElement.attr("href"));

                elements = itemAElement.select("span.item-image>img");
                if (elements.size() > 0) {
                    try {
                        String src=elements.first().attr("src");
                        if(!TextUtils.isEmpty(src)) {
                            URL url = new URL(elements.first().attr("src"));
                            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(),
                                    url.getPath(), url.getQuery(), url.getRef());

                            item.setImageUrl(uri.toASCIIString());
                        }
                    } catch (Throwable ex) {
                        ex.printStackTrace();
                    }
                }
                elements = itemAElement.select("span.item-descr");
                if (elements.size() > 0) {
                    Element itemDescr = elements.first();
                    elements = itemDescr.select("span.item-name");
                    if (elements.size() > 0) {
                        item.setTagTitle(elements.first().text());
                    }

                    elements = itemDescr.select("span.item-title");
                    if (elements.size() > 0) {
                        item.setTitle(elements.first().text());
                    }
                }
                elements = itemAElement.select("span[data-show_id]");
                if (elements.size() > 0) {
                    item.setId(elements.first().attr("data-show_id"));
                }
            }

            if (TextUtils.isEmpty(item.getId()) && TextUtils.isEmpty(item.getTitle())) continue;
            res.add(item);
        }
        return res;
    }
}
