package org.softeg.carambatvapi;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Артём on 26.05.2014.
 */
public class ListInfo implements Parcelable {
    private int from;
    private int max;

    public ListInfo(){

    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public static final Creator<ListInfo> CREATOR
            = new Creator<ListInfo>() {
        public ListInfo createFromParcel(Parcel in) {
            return new ListInfo(in);
        }

        public ListInfo[] newArray(int size) {
            return new ListInfo[size];
        }
    };

    protected ListInfo(Parcel parcel) {
        from = parcel.readInt();
        max = parcel.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(from);
        parcel.writeInt(max);
    }
}
