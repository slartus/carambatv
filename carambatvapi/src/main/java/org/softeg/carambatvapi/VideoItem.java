package org.softeg.carambatvapi;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.awt.font.TextAttribute;
import java.io.Serializable;
import java.util.ArrayList;


/*
 * Created by Артём on 25.05.2014.
 */
public class VideoItem implements Parcelable, Serializable {
    private CharSequence id;
    private CharSequence vId;
    private CharSequence previewImgUrl;
    private CharSequence channelUrl;
    private CharSequence dateString;
    private CharSequence channelTitle;
    private CharSequence title;
    private CharSequence url;
    private VideoSummary videoSummary = new VideoSummary();
    private ArrayList<Quality> qualities = new ArrayList<>();
    private String defaultBitrate;

    private Boolean m_IsYoutube = false;

    public VideoItem() {

    }

    public CharSequence getPreviewImgUrl() {
        return previewImgUrl;
    }

    public void setPreviewImgUrl(CharSequence previewImgUrl) {
        this.previewImgUrl = previewImgUrl;
    }

    public CharSequence getTitle() {
        return title;
    }

    public CharSequence getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(CharSequence channelTitle) {
        this.channelTitle = channelTitle;
    }

    public void setTitle(CharSequence title) {
        this.title = title;
    }

    public CharSequence getId() {
        return id;
    }

    public void setId(CharSequence id) {
        this.id = id;
    }

    public CharSequence getChannelUrl() {
        return channelUrl;
    }

    public void setChannelUrl(CharSequence channelUrl) {
        this.channelUrl = channelUrl;
    }

    public CharSequence getUrl() {
        return url;
    }

    public void setUrl(CharSequence url) {
        this.url = url;
    }

    public VideoSummary getVideoSummary() {
        return videoSummary;
    }

    public CharSequence getDateString() {
        return dateString;
    }

    public void setDateString(CharSequence dateString) {
        this.dateString = dateString;
    }

    public static final Creator<VideoItem> CREATOR
            = new Creator<VideoItem>() {
        public VideoItem createFromParcel(Parcel in) {
            return new VideoItem(in);
        }

        public VideoItem[] newArray(int size) {
            return new VideoItem[size];
        }
    };

    protected VideoItem(Parcel parcel) {
        id = parcel.readString();
        vId = parcel.readString();
        m_IsYoutube = parcel.readByte() == 1;
        previewImgUrl = parcel.readString();
        channelUrl = parcel.readString();
        dateString = parcel.readString();
        channelTitle = parcel.readString();
        title = parcel.readString();
        url = parcel.readString();
        defaultBitrate = parcel.readString();
        videoSummary = new VideoSummary();
        videoSummary.setLikesCount(parcel.readInt());
        videoSummary.setViewsCount(parcel.readInt());
        videoSummary.setCommentsCount(parcel.readInt());

        int arraysCount = parcel.readInt();
        for (int i = 0; i < arraysCount; i++) {
            qualities.add(new Quality(parcel));
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static String getValueOrEmpty(CharSequence str) {
        if (str == null)
            return null;
        return str.toString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getValueOrEmpty(id));
        parcel.writeString(getValueOrEmpty(vId));
        parcel.writeByte(m_IsYoutube ? (byte) 1 : (byte) 0);
        parcel.writeString(getValueOrEmpty(previewImgUrl));
        parcel.writeString(getValueOrEmpty(channelUrl));
        parcel.writeString(getValueOrEmpty(dateString));
        parcel.writeString(getValueOrEmpty(channelTitle));
        parcel.writeString(getValueOrEmpty(title));
        parcel.writeString(getValueOrEmpty(url));
        parcel.writeString(getValueOrEmpty(defaultBitrate));
        parcel.writeInt(videoSummary.getLikesCount());
        parcel.writeInt(videoSummary.getViewsCount());
        parcel.writeInt(videoSummary.getCommentsCount());

        parcel.writeInt(getQualities().size());
        for (Quality item : getQualities()) {
            item.writeToParcel(parcel, i);
        }
    }

    public CharSequence getDefaultVideoUrl() {
        if (m_IsYoutube)
            return !TextUtils.isEmpty(defaultBitrate)?defaultBitrate: getQualities().get(0).getFileName();
        if (getQualities().size() > 0) {
            for (Quality quality : qualities) {
                if (defaultBitrate == null)
                    return getFilePath(vId, quality.getFileName());
                if (defaultBitrate.equals(quality.getHeight()))
                    return getFilePath(vId, quality.getFileName());
            }
        }
        if (defaultBitrate == null)
            return null;
        return getFilePath(vId, defaultBitrate + ".mp4");
    }

    public static String getFilePath(CharSequence vid, CharSequence fileName) {
        return String.format("http://video1.carambatv.ru/v/%s/%s", vid, fileName);
    }


    public ArrayList<Quality> getQualities() {
        return qualities;
    }

    public CharSequence getvId() {
        return vId;
    }

    public void setvId(CharSequence vId) {
        this.vId = vId;
    }

    public void setDefaultBitrate(String defaultBitrate) {
        this.defaultBitrate = defaultBitrate;
    }

    public void setYoutubeVId(String youtubeUrl) {
        this.vId = youtubeUrl;
        m_IsYoutube = true;
    }

    public boolean isYoutube() {
        return m_IsYoutube;
    }

}
