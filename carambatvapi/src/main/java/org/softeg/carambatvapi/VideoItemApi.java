package org.softeg.carambatvapi;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.softeg.carambatvapi.common.Http;

import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Created by Артём on 26.05.2014.
 */
public class VideoItemApi {
    public static Boolean isVideoUrl(String url) {
        String[] patterns = {"video1.carambatv.ru/v/\\d+",
                "http://video1.carambatv.ru/player/ve/player.swf?exc=.*",
                "carambatv.ru/[^/]*/[^/]*/.+"};
        for (String pattern : patterns) {
            if (Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(url).find())
                return true;
        }
        return false;
    }

    public static void getVideoInfo(VideoItem videoItem) throws Exception {

        if (TextUtils.isEmpty(videoItem.getvId()))
            tryGetVideoVId(videoItem);
        if(videoItem.isYoutube())
            return;
        parseVideoInfo(videoItem, videoItem.getvId());

    }

    private static void tryGetVideoVId(VideoItem videoItem) throws Exception {
        Matcher m = Pattern.compile("video1.carambatv.ru(?:%2F|/)v(?:%2F|/)(\\d+)", Pattern.CASE_INSENSITIVE)
                .matcher(videoItem.getUrl());
        if (m.find()) {
            videoItem.setvId(m.group(1));
            return;
        }

        String page = Http.getPage(videoItem.getUrl(), "UTF-8");
         m = Pattern.compile("<meta property=\"og:video\" content=\"[^\"]*video1.carambatv.ru(?:%2F|/)v(?:%2F|/)(\\d+)", Pattern.CASE_INSENSITIVE)
                .matcher(page);
        if (m.find()) {
            videoItem.setvId(m.group(1));
        } else {
            m = Pattern.compile("<iframe[^>]*src=\"[^\"]*youtube.com/embed/([^\"/]*)\"", Pattern.CASE_INSENSITIVE)
                    .matcher(page);
            if (m.find()) {
                videoItem.setYoutubeVId(m.group(1));
                YoutubeApi.parse(videoItem,m.group(1));
            } else
                throw new Exception("Не могу получить vid для видео " + videoItem.getUrl());
        }

    }

    public static CharSequence getId(CharSequence url) throws Exception {
        Matcher m = Pattern.compile("video1.carambatv.ru/v/(\\d+)", Pattern.CASE_INSENSITIVE)
                .matcher(URLDecoder.decode(url.toString(), "UTF-8"));
        if (!m.find())
            return null;

        return m.group(1);
    }

    public static void parseVideoInfo(VideoItem carambaInfo, CharSequence id) throws Exception {
        String page = Http.getPage(String.format("http://video1.carambatv.ru/v/%s/videoinfo.js", id), "UTF-8");
        JSONObject jObj = new JSONObject(page);
        carambaInfo.setId(id);
        carambaInfo.setTitle(jObj.getString("title"));

        if (jObj.has("qualities")) {
            JSONArray jarray = jObj.getJSONArray("qualities");

            for (int i = 0; i < jarray.length(); i++) {
                JSONObject jitem = jarray.getJSONObject(i);

                Quality format = new Quality();
                format.setHeight(jitem.getString("height"));
                format.setFileName(jitem.getString("fn"));
                format.setHd("1".equals(jitem.getString("hd")));
                carambaInfo.getQualities().add(format);
            }
        }

        if (jObj.has("default_bitrate")) {
            carambaInfo.setDefaultBitrate(jObj.getString("default_bitrate"));
        }
    }
}
