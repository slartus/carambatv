package org.softeg.carambatvapi;

import android.text.TextUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.softeg.carambatvapi.common.Http;
import org.softeg.carambatvapi.common.IntegerExtensions;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Артём on 25.05.2014.
 */
public class VideoListApi {
    public static List<VideoItem> parseFromUrl(String url) throws Exception {
        String page = Http.getPage(url, "UTF-8");
        return parsePage(page);
    }


    public static List<VideoItem> parsePage(String page) throws Exception {

        Document doc = Jsoup.parse(page);
        Elements divItems = doc.select("div.items>div.item");

        return parse(divItems);
    }

    public static List<VideoItem> parse(Elements divItems) throws MalformedURLException, URISyntaxException {
        List<VideoItem> res = new ArrayList<>();
        for (Element itemElement : divItems) {
            VideoItem item = new VideoItem();
            item.setId(itemElement.attr("data-post_id"));

            Elements elements = itemElement.select("a.item-image");
            if (elements.size() > 0) {
                Element imgElement = elements.first();
                item.setUrl(imgElement.attr("href"));
                elements = imgElement.select("img");
                if (elements.size() > 0) {
                    try {
                        String imageUrl = elements.first().attr("src");
                        if (imageUrl.startsWith("//"))
                            imageUrl = "http:" + imageUrl;
                        URL url = new URL(imageUrl);
                        URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(),
                                url.getPath(), url.getQuery(), url.getRef());

                        item.setPreviewImgUrl(uri.toASCIIString());
                    }catch (MalformedURLException ignored){

                    }
                }
            }
            elements = itemElement.select("span.item-date");
            if (elements.size() > 0)
                item.setDateString(elements.first().text());
            elements = itemElement.select("a.item-name");
            if (elements.size() > 0) {
                item.setChannelUrl(elements.first().attr("href"));
                item.setChannelTitle(elements.first().text());
            }
            elements = itemElement.select("a.item-title");
            if (elements.size() > 0) {
                item.setUrl(elements.first().attr("href"));
                item.setTitle(elements.first().text());
            }

            elements = itemElement.select("span.item-summary");
            if (elements.size() > 0) {
                Element summaryElement = elements.first();
                elements = summaryElement.select("span.like");
                if (elements.size() > 0)
                    item.getVideoSummary().setLikesCount(IntegerExtensions.tryParse(elements.first().text().replace(" ", ""), 0));

                elements = summaryElement.select("span.views");
                if (elements.size() > 0)
                    item.getVideoSummary().setViewsCount(IntegerExtensions.tryParse(elements.first().text().replace(" ", ""), 0));

//                elements = summaryElement.select("span.comments");
//                if (elements.size() > 0)
//                    item.getVideoSummary().setViewsCount(IntegerExtensions.tryParse(elements.first().text(), 0));

            }
            if (TextUtils.isEmpty(item.getId()) && TextUtils.isEmpty(item.getTitle())) continue;
            res.add(item);
        }
        return res;
    }
}
