package org.softeg.carambatvapi;

import java.io.Serializable;

/**
 * Created by Артём on 25.05.2014.
 */
public class VideoSummary implements Serializable {
    private int likesCount;
    private int viewsCount;
private int commentsCount;
    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(int viewsCount) {
        this.viewsCount = viewsCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }
}
