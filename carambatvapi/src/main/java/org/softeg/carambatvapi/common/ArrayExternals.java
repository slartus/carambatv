package org.softeg.carambatvapi.common;

/*
 * Created by slartus on 29.05.2014.
 */
public class ArrayExternals {
    public static int indexOf(int needle, int[] haystack) {
        for (int i = 0; i < haystack.length; i++) {
            if (haystack[i] == needle) return i;
        }

        return -1;
    }
}
