package org.softeg.carambatvapi.common;

import android.text.TextUtils;
import android.util.Log;



import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.util.List;

/*
 * Created by slinkin on 04.02.14.
 */
public class Http {
    private static final String TAG = "org.softeg.northplayer.common.Http";



    public static String httpPost(CharSequence url,List<NameValuePair> nvps, String encoding) throws Exception {

        if (url != null)
            Log.i(TAG, url.toString());

        HttpClient client = new DefaultHttpClient();

        HttpPost request = new HttpPost(url.toString());
        request.setEntity(new UrlEncodedFormEntity(nvps, encoding));

        request.setHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36");
        HttpResponse response = client.execute(request);
        checkStatus(response.getStatusLine());
        String infoStr = null;
        ByteArrayOutputStream output = null;
        try {
            output = new ByteArrayOutputStream();
            response.getEntity().writeTo(output);
            infoStr = output.toString(encoding);
        } finally {
            if (output != null)
                output.close();
        }
        if (TextUtils.isEmpty(infoStr))
            throw new Exception("Сервер вернул пустую страницу");
        return infoStr;
    }


    public static String getPage(CharSequence url, String encoding) throws Exception {
        if (url != null)
            Log.i(TAG, url.toString());

        HttpClient client = new DefaultHttpClient();

        HttpGet request = new HttpGet(url.toString());
        HttpResponse response = client.execute(request);
        checkStatus(response.getStatusLine());
        String infoStr = null;
        ByteArrayOutputStream output = null;
        try {
            output = new ByteArrayOutputStream();
            response.getEntity().writeTo(output);
            infoStr = output.toString(encoding);
        } finally {
            if (output != null)
                output.close();
        }
        if (TextUtils.isEmpty(infoStr))
            throw new Exception("Сервер вернул пустую страницу");
        return infoStr;
    }

    private static void checkStatus(StatusLine status) throws Exception {
        int statusCode = status.getStatusCode();
        if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_PARTIAL_CONTENT) {
            if (statusCode != 200 && statusCode != 300) {
                throw new Exception(getReasonPhrase(statusCode, status.getReasonPhrase()));
            }
        }
    }


    public static String getReasonPhrase(int code, String defaultPhrase) {
        switch (code) {
            case 404:
                return "Страница не найдена";
            case 500:
                return "Внутренняя ошибка сервера";
            case 501:
                return "Не реализовано";
            case 502:
                return "Плохой шлюз";
            case 503:
                return "Сервис недоступен";
            case 504:
                return "Шлюз не отвечает";
            case 505:
                return "Версия HTTP не поддерживается";
            case 506:
                return "Вариант тоже согласован";
            case 507:
                return "Переполнение хранилища";
            case 509:
                return "Исчерпана пропускная ширина канала";
            case 510:
                return "Не расширено";
        }
        return defaultPhrase;
    }
}
