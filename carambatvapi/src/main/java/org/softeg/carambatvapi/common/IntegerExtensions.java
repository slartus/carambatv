package org.softeg.carambatvapi.common;

/**
 * Created by Артём on 26.05.2014.
 */
public class IntegerExtensions {
    public static int tryParse(String text, int defValue){
        try{
            return Integer.parseInt(text);
        }catch (Throwable ex){
            return defValue;
        }
    }
}
