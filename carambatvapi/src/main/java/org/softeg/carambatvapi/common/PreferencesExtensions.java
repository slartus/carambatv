package org.softeg.carambatvapi.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

/**
 * Created by slinkin on 06.02.14.
 */
public class PreferencesExtensions {
    public String getString(Context context, String key, String defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, defaultValue);
    }

    public static int parseInt(SharedPreferences prefs, String key, int defValue) {
        try {
            String res = prefs.getString(key, Integer.toString(defValue));
            if (TextUtils.isEmpty(res)) return defValue;

            return Integer.parseInt(res);
        } catch (Exception ignored) {

        }

        try {
            return prefs.getInt(key, defValue);

        } catch (Exception ignored) {

        }

        return defValue;

    }

    public static int parseInt(Context context, String key, int defValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return parseInt(prefs, key, defValue);
    }
}
