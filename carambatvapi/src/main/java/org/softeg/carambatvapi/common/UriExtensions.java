package org.softeg.carambatvapi.common;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Patterns;
import android.widget.Toast;



/*
 * Created by slinkin on 06.02.14.
 */
public class UriExtensions {
    public static void showInBrowser(Context context, String url) {
        Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(Intent.createChooser(marketIntent, "Выберите"));
    }

    public static void shareIt(Context context, String url) {
        Intent sendMailIntent = new Intent(Intent.ACTION_SEND);
        sendMailIntent.putExtra(Intent.EXTRA_SUBJECT, url);
        sendMailIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendMailIntent.setData(Uri.parse(url));
        sendMailIntent.setType("text/plain");

        context.startActivity(Intent.createChooser(sendMailIntent, "Поделиться через.."));
    }

    public static void copyLinkToClipboard(Context context, String link) {
        ClipboardBridge.setText(context, link);
        Toast.makeText(context, "Ссылка скопирована в буфер обмена", Toast.LENGTH_SHORT).show();
    }

    public static void showSelectActionDialog(final Context context, final String url) {

        CharSequence[] choices = {"Открыть в", "Поделиться", "Скопировать в буфер"};
        new AlertDialog.Builder(context)
                .setTitle(url)
                .setSingleChoiceItems(choices, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        switch (i) {
                            case 0:
                                showInBrowser(context, url);
                                break;
                            case 1:
                                shareIt(context, url);
                                break;
                            case 2:
                                copyLinkToClipboard(context, url);
                                break;
                        }
                    }
                })
                .setNegativeButton("Отмена", null)
                .setCancelable(true)
                .create().show();
    }
}
