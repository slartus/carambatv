package org.softeg.videoplayer;

import android.app.Activity;
import android.view.KeyEvent;
import android.widget.MediaController;


/*
 * Created by Артём on 25.05.2014.
 */
public class ExtMediaController extends MediaController {

    public interface VisibleChangeListener{
        void onShow();
        void onHide();
    }

    public ExtMediaController(Activity activity, Boolean useFastForward) {
        super(activity,useFastForward);
    }

    @Override
    public void show() {
        super.show();

        if(getContext() instanceof VisibleChangeListener){
            ((VisibleChangeListener)getContext()).onShow();
        }

    }

    @Override
    public void hide() {
        super.hide();

        if(getContext() instanceof VisibleChangeListener){
            ((VisibleChangeListener)getContext()).onHide();
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK){
            ((PlayerActivity) getContext()).finish();
            return true;
        }
        return super.dispatchKeyEvent(event);
    }
}
