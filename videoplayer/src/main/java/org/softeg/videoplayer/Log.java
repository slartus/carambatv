package org.softeg.videoplayer;

import android.app.AlertDialog;
import android.content.Context;

/**
 * Created by slartus on 29.05.2014.
 */
public class Log {
    private static final String TAG = "org.softeg.aycaramba.common.Log";

    public static void e(Context context, Throwable ex) {
        android.util.Log.e(TAG, ex.toString());
        tryShowError(context, ex);
    }

    private static void tryShowError(Context context, Throwable ex) {
        if (context == null) return;
        try {
            new AlertDialog.Builder(context)
                    .setTitle("Ошибка")
                    .setMessage(ex.getMessage())
                    .create().show();
        } catch (Throwable ignored) {

        }
    }
}
