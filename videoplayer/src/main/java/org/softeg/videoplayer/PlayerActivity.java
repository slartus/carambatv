package org.softeg.videoplayer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import org.softeg.videoplayer.hider.Hider;


/*
 * Created by Артём on 26.05.2014.
 */
public class PlayerActivity extends Activity
        implements ExtMediaController.VisibleChangeListener, VideoFormatsFragment.OnVideoInfoSelectListener {


    public static final String PROTECTED_BUNDLE_KEY = "PROTECTED_BUNDLE_KEY";
    public static final String VIDEO_ITEM_KEY = "VIDEO_ITEM_KEY";
    private Hider mSystemUiHider;

    private Bundle mArgs = new Bundle();


    @SuppressWarnings("ResourceType")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.player_activity);



        setRequestedOrientation(parseInt(getApplicationContext(),
                "screen_rotation", ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED));
        if (getActionBar() != null) {
            getActionBar().setDisplayShowHomeEnabled(false);
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        systemUiHiderSetup();
        if (getIntent().getExtras() != null)
            mArgs.putAll(getIntent().getExtras());
        if (savedInstanceState != null)
            mArgs.putAll(savedInstanceState);

        VideoInfo videoInfo = null;
        if (mArgs.containsKey(PROTECTED_BUNDLE_KEY)) {
            videoInfo = mArgs.getBundle(PROTECTED_BUNDLE_KEY).getParcelable(VIDEO_ITEM_KEY);
        } else if (getIntent().getData() != null) {
            CharSequence intentUrl = getIntent().getData().toString();
            CharSequence intentTitle = getIntent().getData().getPath();
            if (getIntent().getData().getPathSegments().size() > 0)
                intentTitle = getIntent().getData().getPathSegments().get(getIntent().getData().getPathSegments().size() - 1);
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("com.android.extra.filename"))
                intentTitle = getIntent().getExtras().getString("com.android.extra.filename");
            videoInfo = new VideoInfo(intentTitle, intentUrl);
        }

        assert videoInfo != null;
        setTitle(videoInfo.getTitle());
        mArgs.putParcelable(VIDEO_ITEM_KEY, videoInfo);
        createPlayerFragment();

        createActionMenu(videoInfo);
    }

    private void createPlayerFragment() {
        VideoInfo videoInfo = mArgs.getParcelable(VIDEO_ITEM_KEY);
        PlayerFragment details = PlayerFragment.newInstance(videoInfo.getUrl(), 0);

        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_player, details).commit();
    }

    public static void show(Context context, VideoInfo videoInfo) {
        Intent intent = new Intent(context, PlayerActivity.class);

        intent.putExtra(VIDEO_ITEM_KEY, (Parcelable) videoInfo);
        context.startActivity(intent);
    }

    private void createActionMenu(VideoInfo videoInfo) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MenuFragment mFragment1 = (MenuFragment) fm.findFragmentByTag(MenuFragment.ID);
        if (mFragment1 != null) {
            ft.remove(mFragment1);
        }
        mFragment1 = MenuFragment.newInstance(videoInfo);

        ft.add(mFragment1, MenuFragment.ID);
        ft.commit();

    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_player);
        if (fragment != null) {
            ((PlayerFragment) fragment).stopPlayback();
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return true;
    }

    private void systemUiHiderSetup() {
        final View contentView = findViewById(R.id.fragment_player);


        mSystemUiHider = Hider.getInstance(this, contentView);
        mSystemUiHider.setup();
        mSystemUiHider.setOnVisibilityChangeListener(new Hider.OnVisibilityChangeListener() {
            @Override
            public void onVisibilityChange(boolean visible) {
                if (visible && Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    showMediaController();
                }
            }
        });

    }

    public void fullScreen() {
        mSystemUiHider.hide();
    }

    public void notFullScreen() {
        mSystemUiHider.show();
    }

    public void showMediaController() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_player);
        if (fragment != null) {
            ((PlayerFragment) fragment).showMediaController(2000);
        }

    }

    @Override
    public void onShow() {
        mSystemUiHider.show();

    }

    Handler mHideBtnUpHandler = new Handler();
    Runnable mHideBtnUpRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    @Override
    public void onHide() {
        mHideBtnUpHandler.removeCallbacks(mHideBtnUpRunnable);
        mHideBtnUpHandler.postDelayed(mHideBtnUpRunnable,500);

    }

    @Override
    public void onInfoSelect(VideoInfo videoItem) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        int currentPosition = 0;
        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_player);
        if (fragment != null) {
            currentPosition = ((PlayerFragment) fragment).getCurrentPosition();
            fragmentTransaction.remove(fragment);
        }

        PlayerFragment details = PlayerFragment.newInstance(videoItem.getUrl(), currentPosition);

        fragmentTransaction.add(R.id.fragment_player, details).commit();
    }

    public static final class MenuFragment extends Fragment {

        public static final String ID = "PlayerActivity.MenuFragment";

        public static MenuFragment newInstance(VideoInfo videoInfo) {
            MenuFragment fragment = new MenuFragment();
            Bundle args = new Bundle();
            args.putParcelable(VIDEO_ITEM_KEY, videoInfo);
            fragment.setArguments(args);
            return fragment;
        }

        public MenuFragment() {
            super();
        }

        private Bundle mArgs = new Bundle();

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null)
                mArgs.putAll(getArguments());
            if (savedInstanceState != null)
                mArgs.putAll(savedInstanceState);
            setHasOptionsMenu(true);
        }

        @Override
        public void onSaveInstanceState(Bundle args) {
            args.putAll(mArgs);
            super.onSaveInstanceState(args);
        }

        private VideoInfo getVideoInfo() {
            return (VideoInfo) mArgs.getParcelable(PlayerActivity.VIDEO_ITEM_KEY);
        }

        public void showFormatsDialog() {
            VideoFormatsFragment videoFormatsFragment
                    = VideoFormatsFragment.newInstance(getVideoInfo());
            videoFormatsFragment.show(getFragmentManager(), VideoFormatsFragment.ID);
        }


        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);

            MenuItem item = menu.add("Ориентация экрана")
                    .setIcon(R.drawable.ic_action_screen_rotation)
                    .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            showOrientationsDialog();
                            return true;
                        }
                    });
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

            if (getVideoInfo().getQualities().size() > 1) {
                item = menu.add("Качество")
                        .setIcon(android.R.drawable.ic_menu_view)
                        .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                showFormatsDialog();
                                return true;
                            }
                        });
                item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            }
        }

        private void showOrientationsDialog() {

            CharSequence[] items = {"Портрет", "Ландшафт", "Авто"};
            new AlertDialog.Builder(getActivity())
                    .setTitle("Ориентация экрана")
                    .setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            switch (i) {
                                case 0:
                                    saveOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                                    break;
                                case 1:
                                    saveOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                                    break;
                                case 2:
                                    saveOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                                    break;
                            }
                        }
                    })
                    .create().show();
        }

        private void saveOrientation(int orientation) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            prefs.edit().putInt("screen_rotation", orientation).commit();
            getActivity().setRequestedOrientation(orientation);
        }


    }

    public static int parseInt(SharedPreferences prefs, String key, int defValue) {
        try {
            String res = prefs.getString(key, Integer.toString(defValue));
            if (TextUtils.isEmpty(res)) return defValue;

            return Integer.parseInt(res);
        } catch (Exception ignored) {

        }

        try {
            return prefs.getInt(key, defValue);

        } catch (Exception ignored) {

        }

        return defValue;

    }

    public static int parseInt(Context context, String key, int defValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return parseInt(prefs, key, defValue);
    }
}
