package org.softeg.videoplayer;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

/*
 * Created by Артём on 26.05.2014.
 */
public class PlayerFragment extends Fragment {
    private static final String TAG = "PlayerFragment";


    public static final String VIDEO_POSITION_EXTRA_KEY = "VIDEO_POSITION_EXTRA_KEY";
    public static final String VIDEO_URL_KEY="VIDEO_URL_KEY";

    public static PlayerFragment newInstance(CharSequence videoFileUrl, int position) {
        PlayerFragment f = new PlayerFragment();
        Bundle args = new Bundle();
        args.putCharSequence(VIDEO_URL_KEY, videoFileUrl);
        args.putInt(VIDEO_POSITION_EXTRA_KEY, position);
        f.setArguments(args);

        return f;
    }

    private CharSequence getVideoFileUrl(){
        return mArgs.getCharSequence(VIDEO_URL_KEY);
    }

    private int getSeekPosition(){
        return mArgs.getInt(VIDEO_POSITION_EXTRA_KEY, 5000);
    }

    private Bundle mArgs = new Bundle();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mArgs.putAll(getArguments());
        if (savedInstanceState != null)
            mArgs.putAll(savedInstanceState);
    }

    private void createActionMenu(CharSequence url) {
        FragmentManager fm = getActivity().getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MenuFragment mFragment1 = (MenuFragment) fm.findFragmentByTag(MenuFragment.ID);
        if (mFragment1 != null) {
            ft.remove(mFragment1);
        }
        mFragment1 = MenuFragment.newInstance(url);

        ft.add(mFragment1, MenuFragment.ID);
        ft.commit();

    }

    @Override
    public void onPause() {
        super.onPause();

        mArgs.putInt(VIDEO_POSITION_EXTRA_KEY, videoView.getCurrentPosition());
        videoView.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        videoView.seekTo(getSeekPosition());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        createActionMenu(getVideoFileUrl());
        playVideo();
    }

    @Override
    public void onSaveInstanceState(Bundle args) {
        mArgs.putInt(VIDEO_POSITION_EXTRA_KEY, videoView.getCurrentPosition());
        args.putAll(mArgs);
        super.onSaveInstanceState(args);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.player_fragment, container, false);
        assert v != null;
        videoView = (VideoView) v.findViewById(R.id.videoView);
        videoView.setKeepScreenOn(true);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer pMp) {
                stopProgress();
                if (getSeekPosition() != 0)
                    videoView.seekTo(getSeekPosition());
                assert getActivity() != null;
                ((PlayerActivity) getActivity()).fullScreen();
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer pMp) {
            }
        });

        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int framework_err, int impl_err) {
                int messageId;

                if (framework_err == MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK) {
                    messageId = android.R.string.VideoView_error_text_invalid_progressive_playback;
                } else {
                    messageId = android.R.string.VideoView_error_text_unknown;

                }

                Log.e(getActivity(), new Exception(getString(messageId)));
                return true;
            }
        });

        return v;
    }

    private void stopProgress() {
        if (getProgressPanel() != null)
            getProgressPanel().setVisibility(View.GONE);
    }

    private View getProgressPanel() {
        if (getActivity() == null)
            return null;
        return getActivity().findViewById(R.id.progressPanel);
    }

    public void showMediaController(int timeOut) {
        if (mMediaController != null) {
            mMediaController.show(timeOut);
        }
    }

    public void playVideo() {
        playVideo(getVideoFileUrl());
    }

    private VideoView videoView;
    private  MediaController mMediaController;

    private void playVideo(CharSequence playUrl) {
        try {
            if (playUrl == null)
                return;

            VideoView videoView = getVideoView();

            videoView.setVideoURI(Uri.parse(playUrl.toString()));

            mMediaController = new ExtMediaController(getActivity(),true);

            mMediaController.setMediaPlayer(videoView);

            videoView.setMediaController(mMediaController);

            videoView.setKeepScreenOn(true);

            videoView.requestFocus();

            videoView.start();
        } catch (Throwable ex) {
            Log.e(getActivity(), ex);
        }
    }

    public VideoView getVideoView() {
        return videoView;
    }

    public void stopPlayback() {
        if(videoView!=null)
            videoView.stopPlayback();
    }

    public int getCurrentPosition() {
        return videoView.getCurrentPosition();
    }

    public static final class MenuFragment extends Fragment {
        public static final String ID = "PlayerFragment.MenuFragment";


        public static MenuFragment newInstance(CharSequence url) {
            MenuFragment fragment = new MenuFragment();
            Bundle args = new Bundle();
            args.putCharSequence(VIDEO_URL_KEY, url);
            fragment.setArguments(args);
            return fragment;
        }

        public PlayerActivity getPlayerActivity() {
            return (PlayerActivity) getActivity();
        }

        private Bundle mArgs = new Bundle();

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null)
                mArgs.putAll(getArguments());
            if (savedInstanceState != null)
                mArgs.putAll(savedInstanceState);
            setHasOptionsMenu(true);
        }

        @Override
        public void onSaveInstanceState(Bundle args) {
            args.putAll(mArgs);
            super.onSaveInstanceState(args);
        }

        private CharSequence getUrl(){
            return mArgs.getCharSequence(VIDEO_URL_KEY);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);
            MenuItem item;

            item = menu.add("Ссылка")
                    .setIcon(R.drawable.ic_action_share)
                    .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            showUrlDialog();
                            return true;
                        }
                    });
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }

        private void showUrlDialog() {
            final String url = getUrl().toString();
            CharSequence[] items = {"Открыть в..", "Поделиться ссылкой", "Скопировать ссылку"};
            new AlertDialog.Builder(getActivity())
                    .setTitle("Ссылка..")
                    .setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            switch (i) {
                                case 0:
                                    UriExtensions.showInBrowser(getActivity(), url);
                                    break;
                                case 1:
                                    UriExtensions.shareIt(getActivity(), url);
                                    break;
                                case 2:
                                    UriExtensions.copyLinkToClipboard(getActivity(), url);
                                    break;
                            }
                        }
                    })
                    .create().show();
        }

    }
}
