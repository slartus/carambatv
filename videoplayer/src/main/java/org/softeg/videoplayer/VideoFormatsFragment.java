package org.softeg.videoplayer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;


/*
 * Created by slartus on 13.02.14.
 */
public class VideoFormatsFragment extends DialogFragment {

    public static final String ID = "org.softeg.northplayer.VideoFormatsFragment";


    public interface OnVideoInfoSelectListener {
        public void onInfoSelect(VideoInfo videoItem);
    }

    OnVideoInfoSelectListener dialogResultListener;

    private Bundle mArgs = new Bundle();

    @Override
    public void onCreate(Bundle savedBundle) {
        super.onCreate(savedBundle);
        if (getArguments() != null)
            mArgs.putAll(getArguments());
        if (savedBundle != null)
            mArgs.putAll(savedBundle);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            dialogResultListener = (OnVideoInfoSelectListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onDialogResultListener");
        }
    }

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    static VideoFormatsFragment newInstance(VideoInfo videoItem) {
        VideoFormatsFragment f = new VideoFormatsFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelable(PlayerActivity.VIDEO_ITEM_KEY, videoItem);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final VideoInfo videoItem = mArgs.getParcelable(PlayerActivity.VIDEO_ITEM_KEY);
        assert videoItem != null;
        CharSequence[] titles = new CharSequence[videoItem.getQualities().size()];
        int i = 0;
        for (VideoInfo format : videoItem.getQualities()) {
            titles[i++] = format.getTitle();
        }
        assert getActivity() != null;
        return new AlertDialog.Builder(getActivity())
                .setTitle("Качество видео ")
                .setSingleChoiceItems(titles, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        dialogResultListener.onInfoSelect(videoItem.getQualities().get(i));

                    }
                })
                .create();
    }

}
