package org.softeg.videoplayer;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * Created by slartus on 29.05.2014.
 */
public class VideoInfo implements Parcelable, Serializable  {
    private CharSequence title;
    private CharSequence url;

    private ArrayList<VideoInfo> qualities=new ArrayList<>();
    public VideoInfo(){

    }

    public VideoInfo(CharSequence title,CharSequence url){
        this.title = title;
        this.url = url;
    }

    public CharSequence getTitle() {
        return title;
    }

    public void setTitle(CharSequence title) {
        this.title = title;
    }

    public CharSequence getUrl() {
        return url;
    }

    public void setUrl(CharSequence url) {
        this.url = url;
    }

    public static final Creator<VideoInfo> CREATOR
            = new Creator<VideoInfo>() {
        public VideoInfo createFromParcel(Parcel in) {
            return new VideoInfo(in);
        }

        public VideoInfo[] newArray(int size) {
            return new VideoInfo[size];
        }
    };

    public VideoInfo(Parcel parcel) {
        title = parcel.readString();
        url = parcel.readString();
        int arraysCount = parcel.readInt();
        for (int i = 0; i < arraysCount; i++) {
            qualities.add(new VideoInfo(parcel));
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static String getValueOrEmpty(CharSequence str) {
        if (str == null)
            return null;
        return str.toString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getValueOrEmpty(title));
        parcel.writeString(getValueOrEmpty(url));

        parcel.writeInt(getQualities().size());
        for (VideoInfo item : getQualities()) {
            item.writeToParcel(parcel, i);
        }
    }

    public ArrayList<VideoInfo> getQualities() {
        return qualities;
    }

    public void setQualities(ArrayList<VideoInfo> qualities) {
        this.qualities = qualities;
    }
}
