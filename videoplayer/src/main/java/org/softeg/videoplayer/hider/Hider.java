package org.softeg.videoplayer.hider;

import android.app.Activity;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;

/**
 * Created by Артём on 25.05.2014.
 */
public class Hider {

    protected Activity mActivity;
    protected View mAnchorView;

    public static Hider getInstance(Activity activity, View anchorView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return new Hider_v19(activity, anchorView);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            return new Hider_v16(activity, anchorView);
        }
        return new Hider(activity, anchorView);
    }

    public void setup() {
        mAnchorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int vis) {
                if ((vis & mTestFlags) != 0) {
                    mOnVisibilityChangeListener.onVisibilityChange(false);
                } else {
                    mOnVisibilityChangeListener.onVisibilityChange(true);
                }
            }
        });
    }

    protected OnVisibilityChangeListener mOnVisibilityChangeListener = sDummyListener;

    /**
     * Flags for {@link android.view.View#setSystemUiVisibility(int)} to use when showing the
     * system UI.
     */
    protected int mShowFlags;
    protected int mTestFlags;
    /**
     * Flags for {@link android.view.View#setSystemUiVisibility(int)} to use when hiding the
     * system UI.
     */
    protected int mHideFlags;

    protected Hider(Activity activity, View anchorView) {
        mActivity = activity;
        mAnchorView = anchorView;

        mShowFlags = View.SYSTEM_UI_FLAG_VISIBLE;
        mHideFlags =
                 View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        mTestFlags= View.SYSTEM_UI_FLAG_LOW_PROFILE;
    }

    public void hide() {
        mActivity.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mAnchorView.setSystemUiVisibility(mHideFlags);
        mActivity.getActionBar().hide();
    }

    public void show() {
        mActivity.getWindow().setFlags(
                0,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mAnchorView.setSystemUiVisibility(mShowFlags);
        mActivity.getActionBar().show();
    }


    public void setOnVisibilityChangeListener(OnVisibilityChangeListener listener) {
        if (listener == null) {
            listener = sDummyListener;
        }

        mOnVisibilityChangeListener = listener;
    }
    /**
     * A dummy no-op callback for use when there is no other listener set.
     */
    private static OnVisibilityChangeListener sDummyListener = new OnVisibilityChangeListener() {
        @Override
        public void onVisibilityChange(boolean visible) {
        }
    };

    public interface OnVisibilityChangeListener {
        /**
         * Called when the system UI visibility has changed.
         *
         * @param visible True if the system UI is visible.
         */
        public void onVisibilityChange(boolean visible);
    }
}
