package org.softeg.videoplayer.hider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.View;

/**
 * Created by Артём on 25.05.2014.
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
public class Hider_v19 extends Hider {

    public Hider_v19(Activity activity,View anchorView){
        super(activity,anchorView);
        mShowFlags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
        mHideFlags =   View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE;
        mTestFlags= View.SYSTEM_UI_FLAG_IMMERSIVE;
    }

    @Override
    public void hide() {
        mAnchorView.setSystemUiVisibility(mHideFlags);
    }

    @Override
    public void show() {
        mAnchorView.setSystemUiVisibility(mShowFlags);
    }
}
